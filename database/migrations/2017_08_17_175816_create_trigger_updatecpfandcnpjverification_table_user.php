<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerUpdatecpfandcnpjverificationTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
              CREATE OR REPLACE FUNCTION CpfAndCnpjVerificationUpdate() RETURNS TRIGGER LANGUAGE plpgsql as $$
              BEGIN
              IF (NEW.cpf IS NULL AND NEW.cnpj IS NULL) THEN
                RAISE SQLSTATE '09000'
                USING MESSAGE = 'Column CPF and CNPJ cannot both be null';
              END IF;

              RETURN NEW;
              END $$;

              CREATE TRIGGER UpdateCpfAndCnpjNotNull BEFORE UPDATE ON users
              FOR EACH ROW EXECUTE PROCEDURE CpfAndCnpjVerificationUpdate();
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP TRIGGER UpdateCpfAndCnpjNotNull ON users");
    }
}
