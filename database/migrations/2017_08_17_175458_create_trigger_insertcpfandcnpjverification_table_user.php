<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerInsertcpfandcnpjverificationTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
          CREATE OR REPLACE FUNCTION InsertCpfAndCnpjVerification() RETURNS TRIGGER language plpgsql as $$
          BEGIN
            IF (NEW.cpf IS NULL AND NEW.cnpj IS NULL) THEN
            RAISE SQLSTATE '09000'
            USING MESSAGE = 'Column CPF and CNPJ cannot both be null';
          END IF;

          return NEW;
          end $$;

          CREATE TRIGGER InsertCpfCnpjNotNull BEFORE INSERT ON users
          FOR EACH ROW EXECUTE PROCEDURE InsertCpfAndCnpjVerification();
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP TRIGGER InsertCpfCnpjNotNull ON users");
    }
}
