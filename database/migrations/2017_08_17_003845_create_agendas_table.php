<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assay_id');
            $table->string('description');
            $table->boolean('active')->default(true);
            $table->text('note');
            $table->date('start_day');
            $table->date('end_day');
            $table->time('start_time');
            $table->time('end_time');
            $table->timestamps();

            $table->foreign('assay_id')->references('id')->on('assays')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendas');
    }
}
