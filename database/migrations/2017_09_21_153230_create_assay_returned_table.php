<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssayReturnedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assay_returned', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assay_id');
            $table->boolean('returned')->default(false);
            $table->timestamps();
            $table->foreign('assay_id')->references('id')->on('assays')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assay_returned')->onDrop('cascade');
    }
}
