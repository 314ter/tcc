<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('clients', function (Blueprint $table) {
             $table->increments('id');
             $table->integer('user_id');
             $table->string('name');
             $table->string('cpf', 14)->nullable();
             $table->string('cnpj', 18)->nullable();
             $table->integer('address_id')->nullable();
             $table->date('birthdate')->nullable();
             $table->enum('marital_status', ['SOLTEIRO', 'CASADO', 'SEPARADO', 'DIVORCIADO', 'VIÚVO'])->nullable();
             $table->enum('gender', ['F', 'M']);
             $table->string('phone', 15)->nullable();
             $table->string('email')->unique();
             $table->boolean('email_confirm')->default(0);
             $table->string('password');
             $table->rememberToken();
             $table->timestamps();

             $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
             $table->foreign('address_id')->references('id')->on('addresses');
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('clients');
     }
}
