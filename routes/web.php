<?php
use App\Mask;
use PhpParser\Comment;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/php', function () {
    return phpinfo();
});

Auth::routes();

Route::post('/register', 'Auth\RegisterController@register');
Route::get('/user/{mail}/{token}/create', 'UserController@createPreUser');
Route::get('/user/register', function () {
    return view('user.register');
})->name('register');

Route::get('/resendMail', function () {
    return view('resendMail');
})->name('resendMail');

Route::post('/resendMail', 'UserController@resendMail');


Route::get('/comentarios/{photo_id}', 'CommentController@index');


Route::post('/preRegister', 'UserController@storePreRegister');
Route::resource('/user', 'UserController');

Route::get('/cliente/{email}/confirmacao', 'UserController@confirmaCliente');
Route::post('/cliente/confirmacao', 'UserController@updatePassword')->name('cliente.confirmacao');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    // ======= User e Studio
    Route::get('/studio', 'StudioController@index')->name('studio');
    Route::get('/studio/profile', 'StudioController@indexProfile')->name('user.profile');
    Route::get('/studio/logout', 'Auth\LoginController@logout')->name('user.logout');

    // ======= Contratos
    Route::get('/studio/contratos', 'ContractController@index')->name('contrato.index');
    Route::get('/studio/contratos/create', 'ContractController@create')->name('contrato.create');
    Route::post('/studio/contratos/store', 'ContractController@store')->name('contrato.store');
    Route::get('/studio/contratos/{id}/edit', 'ContractController@edit')->name('contrato.edit');
    Route::post('/studio/contratos/{id}/edit', 'ContractController@update')->name('contrato.update');
    Route::delete('/studio/contratos/delete', 'ContractController@destroy')->name('contrato.delete');
    // ======= Clientes
    Route::get('/studio/clientes', 'ClientController@index')->name('cliente.index');
    Route::get('/studio/clientes/create', 'ClientController@create')->name('cliente.create');
    Route::post('/studio/clientes/store', 'ClientController@store')->name('cliente.store');
    Route::get('/studio/clientes/{id}/edit', 'ClientController@edit')->name('cliente.edit');
    Route::post('/studio/clientes/{id}/edit', 'ClientController@update')->name('cliente.update');
    Route::delete('/studio/clientes/delete', 'ClientController@destroy')->name('cliente.delete');
    // ======= Tipo de Contratos
    Route::post('/addContractType', 'ContractTypeController@contractTypeStore');
    Route::post('/updateContractType', 'ContractTypeController@contractTypeGet');
    // ======= Ensaios
    Route::get('/studio/ensaios', 'AssayController@index')->name('ensaio.index');
    Route::get('/studio/ensaios/create', 'AssayController@create')->name('ensaio.create');
    Route::post('/studio/ensaios/store', 'AssayController@store')->name('ensaio.store');
    Route::get('/studio/ensaios/{id}/edit', 'AssayController@edit')->name('ensaio.edit');
    Route::post('/studio/ensaios/{id}/edit', 'AssayController@update')->name('ensaio.update');
    Route::get('/studio/ensaios/{id}/galeria', 'AssayController@galery')->name('ensaio.galery');
    Route::get('/studio/ensaios/{id}/fotos', 'AssayController@photos')->name('ensaio.fotos');
    Route::get('/studio/ensaios/{id}/fotos/findComment/{idComment}', 'AssayController@photos');
    Route::post('/studio/ensaios/{id}/fotos', 'AssayController@photosReload')->name('ensaio.fotos.reload');
    Route::delete('/studio/ensaios/delete', 'AssayController@destroy')->name('ensaio.delete');
    Route::post('/uploadImageAssay', 'AssayController@uploadImage')->name('uploadImageAssay');
    Route::post('/removeImageAssay/{image_name}', 'AssayController@removeImage')->name('removeImageAssay');
    Route::post('/updateReleaseAssay', 'AssayController@updateRelease')->name('updateRelease');
    // ======= Mask
    Route::get('/studio/profile/imagem', 'MaskController@index')->name('user.image_configuration');
    Route::post('/uploadImageMask', 'MaskController@store')->name('uploadImageMask');
    Route::post('/uploadConfMask', 'MaskController@update')->name('updateConfMask');

    Route::get('/studio/agendas', 'StudioController@agenda');
});
Route::group(['middleware' => 'client_guest'], function () {
    Route::post('/client_login', 'ClientAuth\LoginController@login')->name('route_cliente_login');
});
Route::group(['middleware' => 'client_auth'], function () {
    Route::get('/client_logout', 'ClientAreaController@logout');
    Route::get('/cliente', 'ClientAreaController@index')->name('cliente');
    Route::get('/cliente/{id_ensaio}', 'ClientAreaController@show');
    Route::post('/cliente/saveSelected', 'ClientAreaController@saveSelected');
    Route::post('/cliente/getSelected', 'ClientAreaController@getSelected');
});

// ======= Comentarios ====== Não deve ficar dentro de um group, porque pode se usado tanto cliente quanto user
Route::post('/studio/newMessage', 'ReadMessageController@index');
Route::post('/studio/readed', 'ReadMessageController@update');
Route::post('/studio/dashboard', 'StudioController@dashboard');
Route::post('/studio/comentario/{id}/fotos', 'CommentController@photosComments')->name('ensaio.comentario.photo');
Route::post('/studio/comentario/store', 'CommentController@store')->name('ensaio.comentario.store');
