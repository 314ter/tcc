<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Read_message extends Model
{
    protected $fillable = ['comment_id', 'readed'];

    public function comments()
    {
        return $this->hasMany("App\Comment");
    }
}
