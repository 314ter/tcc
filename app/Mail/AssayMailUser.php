<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssayMailUser extends Mailable
{
    use Queueable, SerializesModels;
    public $fromMail = 'noreplay@photobooker.com.br';
    public $fromName = 'Equipe [:o:]Photobooker';
    public $user;
    public $list;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $user, string $list)
    {
        $this->user = $user;
        $this->user['list'] = $list;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->fromMail, $this->fromName)
                    ->view('emails/assayListUser', compact($this->user))
                    ->text('emails/clientText')
                    ->subject($this->user['client_name'].' já escolheu!');
    }
}
