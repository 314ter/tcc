<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssayMailClient extends Mailable
{
    use Queueable, SerializesModels;
    public $fromMail = 'noreplay@photobooker.com.br';
    public $fromName = 'Equipe [:o:]Photobooker';
    public $client;
    public $list;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $client, string $list)
    {
        $this->client = $client;
        $this->client['list'] = $list;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->fromMail, $this->fromName)
                    ->view('emails/assayListClient', compact($this->client))
                    ->text('emails/clientText')
                    ->subject('Confirmação de escolha das fotos!');
    }
}
