<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PreRegisterMail extends Mailable
{
    use Queueable, SerializesModels;
    public $fromMail = 'noreplay@photobooker.com.br';
    public $fromName = 'Equipe [:o:]Photobooker';
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->fromMail, $this->fromName)
                    ->view('emails/register', compact($this->user))
                    ->text('emails/registerText')
                    ->subject('E-mail de confirmação!')
        ;
    }
}
