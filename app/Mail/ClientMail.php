<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClientMail extends Mailable
{
    use Queueable, SerializesModels;
    public $fromMail = 'noreplay@photobooker.com.br';
    public $fromName = 'Equipe [:o:]Photobooker';
    public $user;
    public $client;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $user, array $client)
    {
        $this->user = $user;
        $this->client = $client;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->fromMail, $this->fromName)
                    ->view('emails/client', compact($this->user, $this->client))
                    ->text('emails/clientText')
                    ->subject('E-mail de Cadastro!')
        ;
    }
}
