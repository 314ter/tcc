<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreRegister extends Model
{
    protected $fillable = [
      'name', 'email', 'register_token', 'password'
    ];
}
