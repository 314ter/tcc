<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mask extends Model
{
    protected $fillable = [
      'user_id', 'opacity'
    ];

    public function photos()
    {
        return $this->morphMany('App\Photo', 'imageable');
    }
}
