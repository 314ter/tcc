<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

\Carbon\Carbon::setLocale('pt_BR');
class Assay extends Model
{
    protected $fillable = [
      'contract_id', 'name', 'description', 'datetime', 'local', 'released', 'final_date'
  ];

    public function contract()
    {
        return $this->belongsTo('App\Contract');
    }
    public function pick()
    {
        return $this->hasMany('App\Pick');
    }

    public function photos()
    {
        return $this->morphMany('App\Photo', 'imageable');
    }

    public function assay_returned()
    {
        return $this->belongsTo('App\Assay_returned');
    }

    public function getDatetimeAttribute($datetime)
    {
        $diff = Carbon::parse($datetime)->diffForHumans();
        return date('d/m/Y H:i', strtotime($datetime)) .' ('. $diff . ')';
    }

    public function getFinalDateAttribute($final_date)
    {
        if (empty($final_date)) {
            return 'Sem data específica';
        }
        $diff = Carbon::parse($final_date)->diffForHumans();
        return date('d/m/Y', strtotime($final_date)) .' ('. $diff . ')';
    }
}
