<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
      'sender_id', 'sender_type', 'receiver_id', 'receiver_type', 'commentable_id', 'commentable_type', 'content'
    ];

    public function commentable()
    {
        return $this->morphTo();
    }

    public function read_messages()
    {
        return $this->hasOne('App\Read_message');
    }
    // public function photos()
    // {
    //     return $this->morphTo('App\Photo', 'id');
    // }
}
