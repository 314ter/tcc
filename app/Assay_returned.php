<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assay_returned extends Model
{
    protected $table = 'assay_returned';

    protected $fillable = ['assay_id', 'returned'];

    public function assay()
    {
        return $this->belongsTo('App\Assay');
    }
}
