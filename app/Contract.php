<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $fillable = [
      'user_id', 'client_id', 'contract_type_id', 'description', 'text', 'value'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function contract_type()
    {
        return $this->belongsTo('App\Contract_type');
    }

    public function assay()
    {
        return $this->belongsTo('App\Assay');
    }

    public function getValueAttribute($value)
    {
        return "R$".$value;
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = str_replace("R$", "", $value);
        ;
    }
}
