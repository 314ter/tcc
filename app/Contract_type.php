<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract_type extends Model
{
    protected $fillable = [
      'user_id', 'description'
    ];

    public function user()
    {
        $this->belongsTo('App\User');
    }
}
