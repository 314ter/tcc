<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Client extends Authenticatable
{
    use Notifiable;

    /*
    | Indica o guard que será utilizado por essa model
    */
    protected $fillable = [
      'user_id', 'name', 'cpf', 'cnpj', 'address_id', 'marital_status', 'gender', 'birthdate', 'phone', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function address()
    {
        return $this->belongsTo('App\Address');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function contract()
    {
        return $this->belongsTo('App\Client');
    }
    public function contract_type()
    {
        return $this->belongsTo('App\Contract_type');
    }

    public function getBirthdateAttribute($date)
    {
        return date('d/m/Y', strtotime($date));
    }
}
