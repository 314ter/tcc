<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
    'path', 'imageable_id', 'imageable_type', 'image_name',
  ];

    public function imageable()
    {
        return $this->morphTo();
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    // public function assays()
    // {
    //     return $this->belongsTo('App\Assay');
    // }

    // public function address()
    // {
    //     return $this->belongsTo('App\Address');
    // }
    //
    // public function user()
    // {
    //     return $this->belongsTo('App\User');
    // }
}
