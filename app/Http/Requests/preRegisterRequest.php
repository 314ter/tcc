<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class preRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'            =>'required|min:5',
            'email'           =>'required|email|unique:pre_registers',
            'password'        =>'required|min:5',
        ];
    }

    public function messages()
    {
        return [
          'name.min'        => 'O campo Nome contém poucos caracteres, mínimo 5.',
          'email.email'     => 'O campo E-mail não tem um formato válido de e-mail.',
          'email.unique'    => 'Parece que este e-mail já está no nosso pré-cadastro.',
          'password.min'    => 'Sua senha é muito curta, use uma senha maior para melhor segurança'
        ];
    }
}
