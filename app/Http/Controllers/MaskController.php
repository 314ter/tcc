<?php

namespace App\Http\Controllers;

use App\Mask;
use App\User;
use App\Photo;
use Auth;
use Illuminate\Http\Request;
use Response;

class MaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('studio.profile.configuracoes-imagem');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        if ($r->hasFile('file')) {
            $mask = Mask::updateOrCreate(['user_id' => Auth::user()->id], [ 'opacity' => 1.0 ]);
                            // se existir, //criar


            // $mask = new Mask;
            // $mask->user_id = Auth::user()->id;
            // $mask->opacity = '0.0';
            // $mask->save();

            $imageFile = $r->file('file');
            $imageName = uniqid().$imageFile->getClientOriginalName();
            $imageFile->move(public_path('uploads'), $imageName);

            $image = new Photo();
            $image->path = $imageName; //name created
            $image->imageable_id = $mask->id; //id of relationship owner
            $image->imageable_type = "App\Mask"; // model owner
            $image->image_name = $imageFile->getClientOriginalName(); //origina name
            $image->save();
        }
        return response()->json(
          [
            'Status'=>true,
            'Message' => 'Imagem enviada',
            'path' => 'uploads'.DIRECTORY_SEPARATOR.$imageName
          ]
        );
    }

    // public function updateOpacity(Request $r)
    // {
    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mask  $mask
     * @return \Illuminate\Http\Response
     */
    public function show(Mask $mask)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mask  $mask
     * @return \Illuminate\Http\Response
     */
    public function edit(Mask $mask)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mask  $mask
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mask $mask)
    {
        $mask = Mask::whereUser_id(Auth::user()->id)->first();
        $mask->opacity = $request->opacity;

        if ($mask->save()) {
            return Response::json([
            'success' => true,
            'message' => 'Opacidade '.$request->opacity.' salva com sucesso!'
          ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mask  $mask
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mask $mask)
    {
        //
    }
}
