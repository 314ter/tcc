<?php

namespace App\Http\Controllers;

use App\PreRegister;
use Mail;
use App\Mail\PreRegisterMail;
use App\Http\Requests\preRegisterRequest;
use Illuminate\Http\Request;

class PreRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(PreRegister $r)
    public function store(preRegisterRequest $r)
    {
        $user = $r->all();
        $user['password'] = bcrypt($r->password);
        $user['register_token'] = str_random(25);
        PreRegister::create($user);
        $user['image'] = asset('images/LOGO.png');

        Mail::to($user['email'], $user['name'])->send(new PreRegisterMail($user));
        return view('confirm', compact('user'));
        // $mail = Mail::send(['html'=>'emails/register', 'text'=>'emails/registerText'], ['user' => $user], function ($m) use ($user) {
        //     $m->from('noreplay@photobooker.com.br', '[:o:]Photobooker');
        //     $m->to($user['email'], $user['name'])->subject('E-mail de confirmação!');
        // });
        // return PreRegister::all();
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\PreRegister  $preRegister
     * @return \Illuminate\Http\Response
     */
    public function show(PreRegister $preRegister)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PreRegister  $preRegister
     * @return \Illuminate\Http\Response
     */
    public function edit(PreRegister $preRegister)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PreRegister  $preRegister
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PreRegister $preRegister)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PreRegister  $preRegister
     * @return \Illuminate\Http\Response
     */
    public function destroy(PreRegister $preRegister)
    {
        //
    }
}
