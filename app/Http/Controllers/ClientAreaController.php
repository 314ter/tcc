<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Contract;
use App\Assay;
use App\Assay_returned;
use App\Photo;
use App\Pick;
use Auth;
use Response;

use App\Mail\AssayMailClient;
use App\Mail\AssayMailUser;

class ClientAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $client = Auth::guard('web_client')->user();
        $contracts = Contract::whereClient_id($client->id)->get();
        $a_contracts = array();
        foreach ($contracts as $contract) {
            $a_contracts[] = $contract->id;
        }
        $assays = Assay::whereIn('contract_id', $a_contracts)->get();

        return view('client.index', compact('client', 'assays'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Auth::guard('web_client')->user();
        $assay = Assay::findOrFail($id);

        if (!$assay->released) {
            $message = 'Seu fotógrafo ainda não liberou seu ensaio, aguarde!';
            return redirect('cliente')->with('error', $message);
        }

        $contract = Contract::find($assay->contract_id);
        $returned = Assay_returned::whereAssay_id($id)->first();

        if ($returned->returned == true || $client->id != $contract->client_id) {
            $message = 'Você já fez a escolha das fotos. Para acessá-las novamente, deverá pedir que o fotógrafo libere o ensaio. Ou este ensaio não lhe pertence!';
            return redirect('cliente')->with('error', $message);
        }

        $photos = $assay->photos()->get();

        return view('client.photos.index', compact('assay', 'contracts', 'photos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function saveSelected(Request $r)
    {
        if ($r->ajax()) {
            $redirect = false;
            $ensaio = Assay::findOrFail($r->assay_id);

            $pick = Pick::where('assay_id', $ensaio->id)->delete();

            if (isset($r->photos_selected)) {
                foreach ($r->photos_selected as $p) {
                    $pick = new Pick;

                    $pick->assay_id = $ensaio->id;
                    $pick->photo_id = $p;
                    $pick->save();
                }

                $photo = Photo::whereIn("id", $r->photos_selected)->get();
                $list = "";
                for ($i=0; $i < count($photo); $i++) {
                    $list .= $photo[$i]->image_name.", ";
                }
                $list = substr($list, 0, -2);
            }

            if (isset($r->type) && $r->type == 'send') {
                Assay_returned::whereAssay_id($r->assay_id)->update(['returned' => true]);
                $mailClient['email'] = $ensaio->contract->client->email;
                $mailClient['name'] = $ensaio->contract->client->name;
                $mailClient['image'] = asset('images/LOGO.png');

                $mailUser['email'] = $ensaio->contract->user->email;
                $mailUser['name'] = $ensaio->contract->user->name;
                $mailUser['client_name'] = $ensaio->contract->client->name;
                $mailUser['image'] = asset('images/LOGO.png');


                \Mail::to($mailClient['email'], $mailClient['name'])->send(new AssayMailClient($mailClient, $list));
                \Mail::to($mailUser['email'], $mailUser['name'])->send(new AssayMailUser($mailUser, $list));

                // return view('resendMail', compact('pre'));
                $redirect = true;
            }


            return Response::json([
                'success' => true,
                'message' => 'Suas escolhas foram salvas!',
                'redirect' => $redirect
              ]);
        }
    }

    public function getSelected(Request $r)
    {
        if ($r->ajax()) {
            $ensaio = Assay::findOrFail($r->assay_id);

            $selectedIDs = Pick::whereAssay_id($ensaio->id)->pluck('photo_id');
            $selectedNames = Photo::whereIn('id', $selectedIDs)->pluck('image_name');

            return Response::json([
                'selectedIDs' => $selectedIDs,
                'selectedNames' => $selectedNames
              ]);
        }
    }

    public function logout()
    {
        Auth::guard('web_client')->user()->logout();
        return redirect()->route('home');
    }
}
