<?php

namespace App\Http\Controllers;

use Mail;
use App\User;
use App\PreRegister;
use App\Address;
use App\Client;
use App\Mail\PreRegisterMail;

use Illuminate\Http\Request;
use App\Http\Requests\preRegisterRequest;
use App\Http\Requests\preRegisterTokenRequest;
use App\Http\Controllers\Controller;
use Validator;

use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    /**
     * Send an e-mail reminder to the user.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */

    public function sendEmail(Request $r, $id)
    {
        $user = User::findOrFail($id);

        // TODO: EMAIL EXPLICANDO AS FUNÇÕES DO PHOTOBOOKER
    }
    public function resendMail(Request $r)
    {
        $validator = Validator::make($r->all(), [
          'email' => 'required|email',
      ]);

        if ($validator->fails()) {
            return redirect('resendMail')
                      ->withErrors($validator)
                      ->withInput();
        } else {
            // TODO: VERIFICAR SE O EMAIL ESTA NA TABELA PreRegister

            $pre = PreRegister::whereEmail($r->email)->first()->toArray();
            $pre['image'] = asset('images/LOGO.png');
            $pre['emailCode'] = md5($pre['email']);
            Mail::to($pre['email'], $pre['name'])->send(new PreRegisterMail($pre));
            return view('resendMail', compact('pre'));
        }
    }

    public function createPreUser($mail, $token)
    {
        $register = PreRegister::where('register_token', $token)->first();

        $registro = [];
        $registro['email'] = $mail;
        $registro['token'] = $token;

        return redirect()->action('UserController@create', compact('register'));
    }

    public function confirmaCliente($email)
    {
        $client = Client::where('email', $email)->first();
        if ($client && $client->email_confirm == false) {
            if ($client) {
                return view('user.confirmation', compact('client'));
            } else {
                $errors = ['status'=>'error', 'message'=>'Este e-mail não está cadastrado no nosso sistema, caso o problema persista, entre em contato com o seu fotógrafo!'];
                return view('user.confirmation', compact('errors'));
            }
        }
    }

    public function updatePassword(Request $r)
    {
        $client = Client::whereEmail($r->email)->first();
        $client->password = bcrypt($r->password);
        $client->email_confirm = true;
        $client->save();

        return redirect()->route('login');
    }

    public function create(Request $r)
    {
        $user = PreRegister::find($r->register);
        return view('user.register', compact('user'));
    }

    public function store(Request $r)
    {
        $user = $r->all();

        $address = Address::create($user);
        $preUser = PreRegister::where('email', $user['email'])->first();

        $user['password'] = $preUser->password;
        $user['address_id'] = $address->id;
        $user = User::create($user);

        PreRegister::where('email', $user['email'])->delete();

        session([
          'id'    => $user->id,
          'name'  => $user->name
        ]);

        // return view('studio.index', compact('user'));
        return redirect('studio');
    }

    public function storePreRegister(PreRegisterRequest $r)
    {
        $user = $r->all();
        $user['password'] = bcrypt($r->password);
        $user['register_token'] = str_random(25);
        PreRegister::create($user);
        $user['image'] = asset('images/LOGO.png');
        $user['emailCode'] = md5($user['email']);
        Mail::to($user['email'], $user['name'])->send(new PreRegisterMail($user));
        return view('confirm', compact('user'));
    }
}
