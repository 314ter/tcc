<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Client;
use App\Address;
use App\Contract;
use App\Contract_type;
use App\Mail\ClientMail;
use Response;
use Validator;
use Auth;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::whereUser_id(Auth::user()->id)->get();
        return view('studio.client.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('studio.client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $rules = [
          'name' => 'required|max:255',
          'cpf' => 'required_if:cnpj,!=,null|max:14',
          'cnpj' => 'required_if:cpf,!=,null|max:18',
          'postal_code' => 'required|max:9',
          'address_name' => 'required|max:255',
          'district' => 'required|max:255',
          'city' => 'required|max:255',
          'state' => 'required|max:2',
          'country' => 'required|max:2',
          'gender' => 'required',
          'phone' => 'max:15',
          'email' => 'required|email|unique:clients',
        ];

        $error_messages = [
          'name.required' => 'É preciso preencher seu nome',
          'name.max' => 'O campo nome não pode ser maior do que :max caracteres',
          'cpf.required_if' => 'É necessário preencher o CPF ou o CNPJ',
          'cnpj.required_if' => 'É necessário preencher o CPF ou o CNPJ',
          'postal_code.required' => 'É preciso preencher seu CEP',
          'postal_code.max' => 'O campo CEP não pode ser maior do que :max caracteres',
          'address_name.required' => 'É preciso preencher seu Endereço',
          'address_name.max' => 'O campo Endereço não pode ser maior do que :max caracteres',
          'district.required' => 'É preciso preencher seu Bairro',
          'district.max' => 'O campo Bairro não pode ser maior do que :max caracteres',
          'city.required' => 'É preciso preencher seu Cidade',
          'city.max' => 'O campo Cidade não pode ser maior do que :max caracteres',
          'state.required' => 'É preciso selecionar seu Estado',
          'state.max' => 'O campo Estado não pode ser maior do que :max caracteres',
          'country.required' => 'É preciso selecionar seu País',
          'country.max' => 'O campo País não pode ser maior do que :max caracteres',
          'phone.max' => 'O campo Telefone não pode ser maior do que :max caracteres',
          'email.required' => 'É preciso preencher seu Email',
          'email.email' => 'O Email digitado não está no padrão correto',
          'email.unique' => 'O Email digitado já está cadastrado, é necessário utilizar outro',
        ];

        $validation = Validator::make($r->all(), $rules, $error_messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        $client = $r->all();
        $address = Address::create($client);

        $client['user_id'] = Auth::user()->id;
        $client['password'] = bcrypt(str_random(5));
        $client['address_id'] = $address->id;

        $client['image'] = asset('images/LOGO.png');
        $client['emailCode'] = $client['email'];

        $clientNew = Client::create($client);
        $client['email'] = $clientNew->email;
        $client['name'] = $clientNew->name;

        $userNew = $clientNew->user;
        $user['name'] = $userNew['name'];

        Mail::to($client['email'], $client['name'])->send(new ClientMail($user, $client));
        return redirect()->route('cliente.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::findOrFail($id);
        $address = $client->address;

        return view('studio.client.edit', compact('client', 'address'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $client = Client::findOrFail($id);
        $client->update($r->all());
        Address::findOrFail($client->address_id)->update($r->all());

        return redirect()->route('cliente.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $client)
    {
        $client = Client::find($client->id);
        if ($client->delete()) {
            return Response::json([
              'success' => 'Seu cliente foi apagado!'
            ], 200);
        } else {
            return Response::json([
              'error' => 'Não foi possível excluir o seu cliente!'
          ], 401);
        }
    }
}
