<?php

namespace App\Http\Controllers;

use App\Read_message;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Client;
use App\Photo;
use App\Assay;
use App\Comment;
use Response;

class ReadMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $message = [];
        if (!Auth::guard('web_client')->user()) {
            $comment =  Comment::whereReceiver_id(Auth::user()->id)->get();
        // return $comment;
        foreach ($comment as $c) {
            if ($c->read_messages->readed == false) {
                $message['id'] = $c->commentable_id;
                $message['id_comentario'] = $c->id;
                $message['id_assay'] = $c->commentable->imageable_id;
                $message['comentario'] = $c->content;
                $message['cliente'] = $c->commentable->imageable->contract->client->name;
                $message['foto'] = $c->commentable->path;
                break;
            }
        }
        //return $comment[2]->commentable->path; // link para foto
        // return $comment[2]->content; // comentario
        }

        if (sizeof($message) > 0) {
            return response()->json(['status'=>true, 'message' => $message]);
        } else {
            return response()->json(['status'=>false]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Read_message  $read_message
     * @return \Illuminate\Http\Response
     */
    public function show(Read_message $read_message)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Read_message  $read_message
     * @return \Illuminate\Http\Response
     */
    public function edit(Read_message $read_message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Read_message  $read_message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, Read_message $read_message)
    {
        $read = Read_message::where('comment_id', $r->id)->update(['readed' => true]);

        if ($read) {
            return response()->json(['status'=>true, 'message' => 'Mensagem marcada como lida']);
        } else {
            return response()->json(['status'=>false]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Read_message  $read_message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Read_message $read_message)
    {
        //
    }
}
