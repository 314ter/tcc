<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contract;
use App\Contract_type;
use Response;
use Validator;
use Auth;

class ContractTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function contractTypeGet()
    {
        $user_id = Auth::user()->id;
        $contract_types = Contract_type::whereUser_id($user_id)->pluck('description', 'id');
        return Response::json($contract_types);
    }

    public function contractTypeStore(Request $r)
    {
        if ($r->ajax()) {
            $rules = [
              'description_type' => 'required',
            ];

            $validator = Validator::make($r->all(), $rules);
            if ($validator->fails()) {
                return Response::json([
                'errors' => $validator->getMessageBag()->toArray()
            ]);
            } else {
                $description = $r->description_type;
                $user_id = Auth::user()->id;

                $contract_type = new Contract_type();
                $contract_type['user_id'] = $user_id;
                $contract_type['description'] = $description;

                if ($contract_type->save()) {
                    return Response::json([
                      'success' => 'Tipo de contrato salvo com sucesso!'
                    ]);
                }
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
