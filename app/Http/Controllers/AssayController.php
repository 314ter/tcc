<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Assay;
use App\Assay_returned;
use App\Client;
use App\Contract;
use App\Photo;
use App\Pick;
use App\Mask;
use App\User;
use Auth;
use File;
use Response;
use Validator;
use Intervention\Image\ImageManagerStatic as Image;

// use Jaykeren\ImageMoo\Facades\ImageMoo;
// use Spatie\Image;


class AssayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $contracts = Contract::whereUser_id($user_id)->get();


        if (sizeof($contracts) == 0) {
            $message = 'Primeiramente você precisar ter cadastrado um Contrato!';
            return redirect('studio')->with('error', $message);
        }

        $a_contracts = array();
        foreach ($contracts as $contract) {
            $a_contracts[] = $contract->id;
        }
        $assays = Assay::whereIn('contract_id', $a_contracts)->get(); //vou ter varios ensaios

        for ($i=0; $i < count($assays); $i++) {
            if (count($assays[$i]->photos) == 0) {
                $assays[$i]['status'] = "Sem fotos carregadas";
            // } elseif ($assays[$i]->released) {
            } else {
                $assays[$i]['status'] = "Aguardando seleção do cliente";
            }

            // $pick = Pick::whereAssay_id($assays[$i]->id)->get();

            $ret = Assay_returned::whereAssay_id($assays[$i]->id)->get();
            if ($ret[$i]->returned) {
                $assays[$i]['status'] = "Fotos já selecionadas";
            }
        }
        // return $pick = Pick::whereAssay_id($assays[0]->id)->get();

        // foreach ($assays as $key=>$value) {
        //     if (count($assay->photos) == 0) {
        //         $assays['status'] = "Sem fotos carregadas";
        //     } elseif ($assay->released) {
        //         $assays->status = "Aguardando seleção do cliente";
        //     } else {
        //         $pick = Pick::whereAssay_id($assay->id);
        //         if ($pick > 0) {
        //             $assays->status = "Fotos já selecionadas";
        //         }
        //     }
        //
        //
        //     // Aguardando foto/ensaio aberto/fotos selecionadas
        // }


        return view('studio.assay.index', compact('assays'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // TODO: vincular nome do cliente com o nome do ensaio

        $contracts = Contract::whereUser_id(Auth::user()->id)->pluck('description', 'id');
        return view('studio.assay.create', compact('contracts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        // return $r->all();


        if (!$r->local) {
            $r['local'] = "0|0";
        }
        $rules = [
          "name" => 'required|max:255',
          "description" => 'required|max:255',
          "_datetime" => 'required|max:255',
          "date_time" => 'required|max:255',
          "final_date" => 'required|max:255'
        ];

        $error_messages = [
          'description.required' => 'É preciso preencher o nome do contrato',
          'value.required' => 'É preciso preencher um valor para o contrato'
        ];

        $validation = Validator::make($r->all(), $rules, $error_messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }


        $r['user_id'] = Auth::user()->id;
        $r['datetime'] = $r->_datetime . ' ' .$r->date_time;
        $assay = Assay::create($r->all());
        Assay_returned::create(['assay_id'=>$assay->id]);
        return redirect()->route('ensaio.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $assay = Assay::findOrFail($id);

        $assay->date = date('d/m/Y', strtotime($assay->getOriginal('datetime')));
        $assay->hour = date('H:i', strtotime($assay->getOriginal('datetime')));
        $contracts = Contract::pluck('description', 'id');

        return view('studio.assay.edit', compact('assay', 'contracts'));
    }
    public function uploadImage(Request $r)
    {
        $thumb_dir = public_path('/storage/images/thumb/');
        $image_dir = public_path('/storage/images/');

        if ($r->hasFile('file')) {
            $imageFile = $r->file('file');
            $name = $imageFile->getClientOriginalName();
            $ext = $imageFile->getClientOriginalExtension();
            $newName = uniqid().time().'.'.$ext;

            Image::make($imageFile->getRealPath())->resize(null, 1000, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($image_dir. $newName);

            Image::make($imageFile->getRealPath())->resize(null, 400, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($thumb_dir. $newName, 70);

            $photo = new Photo();
            $photo->path = $newName; //name created
            $photo->imageable_id = $r->id; //id of relationship owner
            $photo->imageable_type = "App\Assay"; // model owner
            $photo->image_name = $name; //origina name
            $photo->save();
        }

        return response()->json(['status'=>true, 'message' => 'Imagem enviada']);
    }

    public function removeImage($image_name)
    {
        $image = Photo::whereImage_name($image_name)->first();

        $file = public_path('/storage/images/'.$image->path);
        $fileThumb = public_path('/storage/images/thumb/'.$image->path);
        File::delete($file);
        File::delete($fileThumb);
        if (!File::exists($file)) {
            Photo::whereImage_name($image_name)->first()->delete();
        }
        return response()->json(['status'=>true, 'message' => 'Imagem removida']);
    }

    public function updateRelease(Request $r)
    {
        $assay = Assay::findOrFail($r->id)->update(['released' => $r->released]);
        $message = ($r->released=='true') ? "Ensaio aberto para seu cliente!" : "Ensaio fechado para seu cliente!";
        return response()->json(['released'=>$r->released, 'message' => $message]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $r['datetime'] = $r->_datetime . " " . $r->date_time;
        $ensaio = Assay::findOrFail($id);
        $ensaio->update($r->all());

        // unset($r['_token']);
        // unset($r['_datetime']);
        // unset($r['date_time']);
        //
        // DB::table('assays')
        //     ->where('id', $id)
        //     ->update($r->all());

        return redirect()->route('ensaio.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $assay)
    {
        $assay = Assay::find($assay->id);
        if ($assay->delete()) {
            return Response::json([
              'success' => 'Seu ensaio foi apagado!'
            ], 200);
        } else {
            return Response::json([
              'error' => 'Não foi possível excluir o seu ensaio!'
            ], 401);
        }
    }

    // public function galery($id)
    // {
    //     // TODO: fazser verificação para não deixar outros fotografos entrarem nessa galeria
    //     return view('studio.gallery.index');
    // }

    public function photosReload($id)
    {
        $assay = Assay::findOrFail($id);
        $photos = $assay->photos()->get();
        return $photos;
    }

    public function photos($id)
    {
        // TODO: CRIAR UMA IMAGEM DO USUARIO COM TRANSPARENCIA, DEPOIS USAR O INSERT DELA NAS IMAGENS DO USER

        // $mask = Mask::whereUser_id(Auth::user()->id)->first();

        // create new Intervention Image from file and set image full transparent
        //$img = Image::make('public/foo.jpg');
        //$img->opacity(0);
        //->height();
        //->width();

        $assay = Assay::findOrFail($id);
        $contracts = Contract::pluck('description', 'id');
        // $photos = $assay->photos()->get();
        $photos = $assay->photos()->get();

        $mask = Mask::whereUser_id(Auth::user()->id)->first();

        $picks = Pick::whereAssay_id($assay->id)->get();

        $p = "";

        for ($i=0; $i < sizeof($picks); $i++) {
            $photo = Photo::find($picks[$i]->photo_id);
            $p .= $photo->image_name .", ";
        }

        $list = substr($p, 0, -2);

        // TODO: VERIFICAR SE ENSAIO DEVE TER MARCADAGUA
        //if ($mask==true && 1==1) {
            // $watermark_image = $mask->photos()->first(); //image to use opacity
            // $watermark = Image::make(public_path("/uploads/$watermark_image->path"))->opacity(40);
            // foreach ($photos as $photo) {
            //     $originalPath = $photo->path;
            //     $image = Image::make(public_path("uploads/$photo->path"));
            //     $image->insert($watermark, 'center');
            //     // return $image->opacity(.4);
            //     $image->save(public_path("uploadsW/$originalPath"), 100);
            //     // $oi[] = $photo->image_name;
            // }

            // $mask = $mask->photos()->first();
            // foreach ($photos as $photo) {
            //     $pathImage = public_path("uploads/$photo->path");
            //     $pathLogo = public_path("uploads/$mask->path");
            //     $image = new Image();
            //     $image->fromFile($pathImage);
            //     $image->overlay($pathLogo, 'bottom left', 0.4)->toFile(public_path("uploadsW/$photo->path"));
                // $logo = Image::load($pathLogo);
                // $image->watermark($pathLogo)->watermarkOpacity(50)->save();
                // $image->sepia()->save();
                // $logo->show()->opacity(50);
                // $watermark = $image->watermark($logo, 'center');
                // $watermark->save(public_path("uploadsW/$photo->path"));
            // }


            // $image->watermark('watermark.png')
            //       ->watermarkOpacity(50);


            // return $oi;
            // $image = Image::make(public_path('/uploads/59b9298070995a.jpg'));
            // $image->insert(public_path('/images/LOGO2.png'), 'center');
            // $image->opacity(.4);
            // $image->save(public_path('/uploadsW/a.jpg'), 100);
        // }
        // if(isset()){
        //
        //   return json_encode($photos);
        // }

        return view('studio.photos.index', compact('assay', 'contracts', 'photos', 'list'));
    }
}
