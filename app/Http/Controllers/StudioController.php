<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\User;
use App\Client;
use App\Assay;
use App\Address;
use App\Contract;
use App\Comment;
use App\Pick;
use App\Contract_type;
use App\Mail\ClientMail;
use Response;
use Validator;
use Auth;
use Carbon\Carbon;

class StudioController extends Controller
{
    public function __construct()
    {
        /*
  | Esta linha protege todos os métodos que estão dentro da classe
  | para que somente usuários autenticados possam acessar
  | caso contrario, o sistema força e fazer login
  */
    }

    public function index()
    {
        return view('studio.index');
    }

    public function dashboard()
    {
        $user_id = Auth::user()->id;
        $contracts = Contract::whereUser_id($user_id)->get();
        $a_contracts = array();
        foreach ($contracts as $contract) {
            $a_contracts[] = $contract->id;
        }
        $assays = Assay::whereIn('contract_id', $a_contracts)->orderBy('datetime', 'asc')->get(); //vou ter varios ensaios

        foreach ($assays as $a) {
            switch ($originalMonth = Carbon::parse($a->getOriginal('datetime'))->format('F')) {
              case 'January':
                $message['mes'][] = "JAN";
                break;
              case 'February':
                $message['mes'][] = "FEV";
                break;
              case 'March':
                $message['mes'][] = "MAR";
                break;
              case 'April':
                $message['mes'][] = "ABR";
                break;
              case 'May':
                $message['mes'][] = "MAI";
                break;
              case 'June':
                $message['mes'][] = "JUN";
                break;
              case 'July':
                $message['mes'][] = "JUL";
                break;
              case 'August':
                $message['mes'][] = "AGO";
                break;
              case 'September':
                $message['mes'][] = "SET";
                break;
              case 'October':
                $message['mes'][] = "OUT";
                break;
              case 'November':
                $message['mes'][] = "NOV";
                break;
              case 'December':
                $message['mes'][] = "DEZ";
                break;
            }
        }
        $message['qtdMes'] = array_count_values($message['mes']); // returna soma dos meses iguais

        // ======================

        $comentario = Comment::whereReceiver_type('App\User')->whereReceiver_id($user_id)->orderBy('created_at', 'desc')->get();
        $message['c_content'] = [];
        foreach ($comentario as $c) {
            $message['c_content'][] = $c->content;
            $message['c_client'][] = $c->commentable->imageable->contract->client->name;
            $message['c_image'][] = $c->commentable->image_name;
        }
        // ======================

        $assays = Assay::whereIn('contract_id', $a_contracts)->whereDate('datetime', '>=', date("Y-m-d"))->orderBy('datetime', 'asc')->get(); //vou ter varios ensaios

        foreach ($assays as $a) {
            switch (Carbon::parse($a->getOriginal('datetime'))->format('l')) {
              case 'Sunday':
                $dia_semana = "Domingo";
                break;
              case 'Monday':
                $dia_semana = "Segunda-feira";
                break;
              case 'Tuesday':
                $dia_semana = "Terça-feira";
                break;
              case 'Wednesday':
                $dia_semana = "Quarta-feira";
                break;
              case 'Thursday':
                $dia_semana = "Quinta-feira";
                break;
              case 'Friday':
                $dia_semana = "Sexta-feira";
                break;
              case 'Saturday':
                $dia_semana = "Sábado";
                break;
            }
            $message['e_date'][] = $dia_semana ." - ". Carbon::parse($a->getOriginal('datetime'))->format('d/m/Y H:i');
            $message['e_client'][] = Contract::findOrFail($a->contract_id)->client->name;
            $message['e_phone'][] = Contract::findOrFail($a->contract_id)->client->phone;
            $message['e_email'][] = Contract::findOrFail($a->contract_id)->client->email;
        }
        // ======================

        $assays = Assay::whereIn('contract_id', $a_contracts)->get();
        $message['p_pick'] = [];
        foreach ($assays as $a) {
            if (Pick::whereAssay_id($a->id)->get()->isNotEmpty()) {
                $message['p_pick'][] = Pick::whereAssay_id($a->id)->get();
                $message['p_assay'][] = $a->name;
                $message['p_client'][] = Contract::findOrFail($a->contract_id)->client->name;
                // $message['p_assay'] = $a->name;
                // $message['p_client'] = $a->client->name;
            }
        }

        return response()->json(['status'=>true, 'message' => $message]);
    }

    public function agenda()
    {
        return view('studio.agenda');
    }

    public function assay()
    {
        return view('studio.assay');
    }

    public function indexProfile()
    {
        $user = User::findOrFail(Auth::user()->id);
        $address = $user->address;
        return view('studio.profile.dados-cadastrais', compact('user', 'address'));
    }

    public function imageConfigurationProfile()
    {
        // $user = User::findOrFail(Auth::user()->id);
        // $address = $user->address;
        // return view('studio.profile.configuracoes-imagem', compact('user', 'address'));
    }
}
