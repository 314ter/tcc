<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Assay;
use App\Photo;
use App\Client;
use App\User;
use App\Contract;
use App\Comment;
use App\Read_message;
use Carbon\Carbon;

\Carbon\Carbon::setLocale('pt_BR');
class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $photo = Photo::find($r->id_photo);
        $contract = Contract::findOrFail($photo->imageable->contract_id);

        if ($r->sender == "App\Client") {
            $sender = "App\Client";
            $receiver = "App\User";
        } else {
            $sender = "App\User";
            $receiver = "App\Client";
        }

        $comment = Comment::create([
          "sender_id" => $contract->user_id,
          "sender_type" => $sender,
          "receiver_id" => $contract->client_id,
          "receiver_type" => $receiver,
          "commentable_id" => $r->id_photo,
          "commentable_type" => 'App\Photo',
          "content" => $r->comment
        ]);

        $read = Read_message::create(["comment_id"=>$comment->id, "readed"=>false]);

        if ($comment) {
            $response["status"] = true;
            // $response[] = ["status"=>true];
        } else {
            $response["status"] = false;
        }
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function photosComments($photo_id)
    {
        $photo = Photo::find($photo_id);
        $comments = $photo->comments()->orderBy('id', 'asc')->get();
        foreach ($comments as $c) {
            if ($c->sender_type == 'App\Client') {
                $sender = Client::find($c->sender_id);
            } else {
                $sender = User::find($c->sender_id);
                Read_message::where('comment_id', $c->id)->update(['readed'=>true]);
            }
            $date = $c->created_at->diffForHumans();
            $sender_type = $c->sender_type;
            $message = $c->content;
            $response[] = ["sender_type"=>$sender_type, "sender"=>$sender, "message"=>$message, "date"=>$date];
        }


        $response["config"] = ["n_messages"=>$comments->count(), "photo_name"=>$photo->image_name];

        return response()->json($response);
    }
}
