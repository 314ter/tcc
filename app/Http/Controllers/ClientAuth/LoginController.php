<?php

namespace App\Http\Controllers\ClientAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Class needed for login and Logout logic
use Illuminate\Foundation\Auth\AuthenticatesUsers;

//Auth facade
use Auth;

class LoginController extends Controller
{
    protected $redirectTo = '/cliente';

    use AuthenticatesUsers;

    protected function guard()
    {
        return Auth::guard('web_client');
    }
}
