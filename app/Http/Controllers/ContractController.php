<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Contract;
use App\Contract_type;
use Response;
use Validator;
use Auth;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $contracts = Contract::whereUser_id($user_id)->get();

        $clients = Client::whereUser_id($user_id)->pluck('name', 'id');
        if (sizeof($clients) == 0) {
            $message = 'Primeiramente você precisar ter cadastrado um Usuário!';
            return redirect('studio')->with('error', $message);
        }

        // return $contracts->contract_type->id;

        return view('studio.contract.index', compact('contracts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_id = Auth::user()->id;
        $clients = Client::whereUser_id($user_id)->pluck('name', 'id');
        $typeContracts = Contract_type::whereUser_id($user_id)->pluck('description', 'id');

        return view('studio.contract.create', compact('clients', 'typeContracts'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        // return $r->all();

        $rules = [
          'description' => 'required|max:255',
          'value' => 'required|max:255',
        ];

        $error_messages = [
          'description.required' => 'É preciso preencher o nome do contrato',
          'value.required' => 'É preciso preencher um valor para o contrato'
        ];

        $validation = Validator::make($r->all(), $rules, $error_messages);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        if ($r->text == null) {
            $r['text'] = "";
        }
        $r['user_id'] = Auth::user()->id;
        $contract = Contract::create($r->all());

        return redirect()->route('contrato.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contract = Contract::findOrFail($id);
        $user_id = Auth::user()->id;
        $clients = Client::whereUser_id($user_id)->pluck('name', 'id');
        $typeContracts = Contract_type::whereUser_id($user_id)->pluck('description', 'id');
        return view('studio.contract.edit', compact('contract', 'clients', 'typeContracts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Contract::findOrFail($id)->update($request->all());
        $contracts = Contract::all();
        return redirect()->route('contrato.index');
        // return view('studio.contract.index', compact('contracts'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $contract)
    {
        $contract = Contract::find($contract->id);
        if ($contract->delete()) {
            return Response::json([
            'success' => 'Seu contrato foi apagado!'
          ], 200);
        } else {
            return Response::json([
            'error' => 'Não foi possível excluir o seu contrato!'
        ], 401);
        }
    }
}
