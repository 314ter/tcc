<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pick extends Model
{
    protected $fillable = [
      'assay_id', 'photo_id'
    ];

    public function assay()
    {
        return $this->belongsTo("App\Assay");
    }

    public function photos()
    {
        return $this->belongsToMany("App\Photos");
    }
}
