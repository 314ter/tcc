$("#btn-close").click(function(){
  open(location, '_self').close();
  return false;
});

function Toast(msg, speed=2000, classe){
  Materialize.toast(msg, speed, classe);
}

$("#postal_code").focusout(function(){
  $.ajax({
    type: "GET",
    url: "https://viacep.com.br/ws/"+$(this).val()+"/json/",
    success: function( msg ) {
      $("[name='address_name'").val(msg.logradouro);
      $("[name='district'").val(msg.bairro);
      $("[name='city'").val(msg.localidade);
      $("[name='state'] option[value='RS']").prop("selected", true);
      $("[name='state']").material_select();
      $("[name='country']").val('BR');
    },
    error: function(msg){
      Toast("Não encontramos o seu CEP, tente novamente, caso persista o erro, digite seus dados manualmente!", 5000, "red-text");
    }
  });
});


$(document).ready(function() {
  $('select').material_select();
  $('.datepicker').pickadate({
    monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    weekdaysFull: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabádo'],
    weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 200, // Creates a dropdown of 15 years to control year,
    today: 'Hoje',
    clear: 'Limpar',
    close: 'Ok',
    format: 'dd/mm/yyyy',
    closeOnSelect: false // Close upon selecting a date,
  });
  $('.timepicker').pickatime({
    default: 'now', // Set default time: 'now', '1:30AM', '16:30'
    fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
    twelvehour: true, // Use AM/PM or 24-hour format
    donetext: 'OK', // text for done-button
    cleartext: 'Limpar', // text for clear-button
    canceltext: 'Cancelar', // Text for cancel-button
    autoclose: false, // automatic close timepicker
    ampmclickable: false, // make AM PM clickable
    // aftershow: function(){} //Function for after opening timepicker
});
  $("#juridicalLabel").click(function(){
    if($("#juridical").prop("checked") == false){
      $("#divCPF").toggleClass('hide');
      $("#divCNPJ").toggleClass('hide');
    }else{
      $("#divCPF").toggleClass('hide');
      $("#divCNPJ").toggleClass('hide')
    }
  });
});

function inputHandler(masks, max, event) {
	var c = event.target;
	var v = c.value.replace(/\D/g, '');
	var m = c.value.length > max ? 1 : 0;
	VMasker(c).unMask();
	VMasker(c).maskPattern(masks[m]);
	c.value = VMasker.toPattern(v, masks[m]);
}

var telMask = ['(99) 9999-99999', '(99) 99999-9999'];
var tel = document.querySelector('#tel');
VMasker(tel).maskPattern(telMask[0]);
tel.addEventListener('input', inputHandler.bind(undefined, telMask, 14), false);

var cpfMask = ['999.999.999-99'];
var doc = document.querySelector('#cpf');
VMasker(doc).maskPattern(cpfMask[0]);
doc.addEventListener('input', inputHandler.bind(undefined, cpfMask, 15), false);

var cnpjMask = ['99.999.999/9999-99'];
var cnpj = document.querySelector('#cnpj');
VMasker(cnpj).maskPattern(cnpjMask[0]);
cnpj.addEventListener('input', inputHandler.bind(undefined, cnpjMask, 19), false);

var postalMask = ['99999-999'];
var postal = document.querySelector('#postal_code');
VMasker(postal).maskPattern(postalMask[0]);
postal.addEventListener('input', inputHandler.bind(undefined, postalMask, 10), false);
