$("#menucollapse").click(function(){
  $('#slideout').toggleClass('on');
});

$( window ).scroll(function() {
  closeMenu();
});

$(".link-back").click(function(){
  window.location.href = $(this).attr('href');
})

function closeMenu(){
    $('#slideout').addClass('on');
}
function Toast(msg, speed=2000, classe){
  Materialize.toast(msg, speed, classe);
}

// =============== STUDIO >>>>> PHOTOS >>>>>> INDEX

// ajax para atualizar o released no bd quando alterar o switch
$("#switchLiberar").click(function(){
  if($(this).prop('checked')){
    released = true;
  }else{
    released = false;
  }
  $.ajax({
    type: "POST",
    url: "/updateReleaseAssay",
    data: {
        'released'  : released,
        'id'        : $(this).val()
      },
    success: function( msg ) {
      Toast(msg.message, 2000, 'assay-toast');
    },
    error: function(msg){
      // $("#apagar").html(msg.responseText);
    }
  });
});
