$("#menucollapse").click(function(){
  $('#slideout').toggleClass('on');
});

$( window ).scroll(function() {
  closeMenu();
});


function closeMenu(){
    $('#slideout').addClass('on');
}
function Toast(msg, speed=2000, classe){
  Materialize.toast(msg, speed, classe);
}

$("#juridicalLabel").click(function(){
  if($("#juridical").prop("checked") == false){
    $("#divCPF").toggleClass('hide');
    $("#divCNPJ").toggleClass('hide');
  }else{
    $("#divCPF").toggleClass('hide');
    $("#divCNPJ").toggleClass('hide')
  }
})

$('select').material_select();
$('.datepicker').pickadate({
  monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
  monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
  weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
  weekdaysFull: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabádo'],
  selectMonths: true, // Creates a dropdown to control month
  selectYears: 200, // Creates a dropdown of 15 years to control year,
  today: 'Hoje',
  clear: 'Limpar',
  close: 'Ok',
  format: 'dd/mm/yyyy',
  closeOnSelect: false // Close upon selecting a date,
});
$('.timepicker').pickatime({
  default: 'now', // Set default time: 'now', '1:30AM', '16:30'
  fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
  twelvehour: false, // Use AM/PM or 24-hour format
  format: "HH:ii",
  vibrate: true,
  donetext: 'OK', // text for done-button
  cleartext: 'Limpar', // text for clear-button
  canceltext: 'Cancelar', // Text for cancel-button
  autoclose: false, // automatic close timepicker
  ampmclickable: false, // make AM PM clickable
  // aftershow: function(){} //Function for after opening timepicker
});

// $.ajaxSetup({
//   headers: {
//       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//   }
// });


// =============== STUDIO >>>>> PHOTOS >>>>>> INDEX

// ajax para atualizar o released no bd quando alterar o switch
$("#switchLiberar").click(function(){
  if($(this).prop('checked')){
    released = true;
  }else{
    released = false;
  }
  $.ajax({
    type: "POST",
    url: "/updateReleaseAssay",
    data: {
        'released'  : released,
        'id'        : $(this).val(),
        '_token' : $('meta[name="csrf-token"]').attr('content')
      },
    success: function( msg ) {
      Toast(msg.message, 2000, 'assay-toast');
    },
    error: function(msg){
      // $("#apagar").html(msg.responseText);
    }
  });
});

$("#link-readed").click(function(){
  var id = $(this).attr("message-id");
  $.ajax({
    type: "POST",
    url: "/studio/readed",
    data: { id : id, _token : $('meta[name="csrf-token"]').attr('content')},
    success: function( msg ) {
      // console.log(msg);
      Toast(msg.message, 3000);
      $(".alert-mew-message").addClass("hide");
    }
  });
});

// ================================================

// =============== STUDIO >>>>> CONTRACT >>>>>> CREATE
$("#juridicalLabel").click(function(){
  if($("#juridical").prop("checked") == false){
    $("#divCPF").toggleClass('hide');
    $("#divCNPJ").toggleClass('hide');
  }else{
    $("#divCPF").toggleClass('hide');
    $("#divCNPJ").toggleClass('hide')
  }
})
// ================================================


$.ajax({
  type: "POST",
  url: "/studio/newMessage",
  data:{'_token' : $('meta[name="csrf-token"]').attr('content')},
  success: function( msg ) {
    if (msg.status == true) {
      $(".alert-mew-message").removeClass("hide");
      $("#client-message").html("<u>"+msg.message.cliente+"</u>");
      $("#message-message").html(msg.message.comentario);
      $("#link-readed").attr("message-id", msg.message.id_comentario);
      $("#link-photo").attr("href", "/studio/ensaios/"+msg.message.id_assay+"/fotos/findComment/"+msg.message.id);
      // console.log(msg.message);
    }
  }
});

$("#postal_code").focusout(function(){
  $.ajax({
    type: "GET",
    url: "https://viacep.com.br/ws/"+$(this).val()+"/json/",
    success: function( msg ) {
      $("[name='address_name'").val(msg.logradouro);
      $("[name='district'").val(msg.bairro);
      $("[name='city'").val(msg.localidade);
      $("[name='state'] option[value='RS']").prop("selected", true);
      $("[name='state']").material_select();
      $("[name='country']").val('BR');
    },
    error: function(msg){
      Toast("Não encontramos o seu CEP, tente novamente, caso persista o erro, digite seus dados manualmente!", 5000, "red-text");
    }
  });
});


// ============ DASHBOARD ========================


var nomeMes = [];
var qtdMes = [];
$.ajax({
  type: "POST",
  url: "/studio/dashboard",
  data: {'_token' : $('meta[name="csrf-token"]').attr('content')},
  success: function( msg ) {
    if (msg.status == true) {
      $.each(msg.message.qtdMes, function(key, value) {
        nomeMes.push(key);
        qtdMes.push(value);
      });
      var LineChartSampleData = {
            labels: nomeMes,
            datasets: [{
            label: "My First dataset",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: qtdMes
        }]
       };
        window.LineChartSample = new Chart(document.getElementById("line-chart-sample").getContext("2d")).Line(LineChartSampleData,{
         responsive:true
      });
      var e = msg.message;
      for(var i=0; i< e.c_content.length; i++){
        var c_html = "<tr>"+
          "<td>"+i+"</td>"+
          "<td>"+e.c_client[i]+"</td>"+
          "<td>"+e.c_content[i]+"</td>"+
          "<td>"+e.c_image[i]+"</td>"+
        "</tr>";
        $("#tbody_last_messages").append(c_html);
      };

      for (var i = 0; i < e.e_date.length; i++) {
        var e_html = "<tr>"+
          "<td>"+e.e_date[i]+"</td>"+
          "<td>"+e.e_client[i]+"</td>"+
          "<td>"+e.e_phone[i]+"</td>"+
          "<td>"+e.e_email[i]+"</td>"+
        "</tr>";
        $("#tbody_last_events").append(e_html);
      }

      for (var i = 0; i < e.p_pick.length; i++) {
        var data = e.p_pick[i][0].created_at;
        data = data.split(" ");
        var novadata = data[0]
        novadata = novadata.split("-");
        novadata = novadata[2]+"/"+novadata[1]+"/"+novadata[0];
        var p_html = "<tr>"+
          "<td>"+e.p_assay[i]+"</td>"+
          "<td>"+e.p_client[i]+"</td>"+
          "<td>"+novadata+"</td>"+
          "<td>"+e.p_pick[i].length+"</td>"+
        "</tr>";
        $("#tbody_last_photos").append(p_html);
      }
    }
  }
});



// ================================================
