@extends('client.studio')
@section('content')
  <link rel="stylesheet" href="{{ asset('css/nanogallery2.min.css')}}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
  <link rel="stylesheet" href="{{ asset('css/chat.css') }}">
  {{ Form::token() }}
  <nav>
    <div class="nav-wrapper">
      <ul id="nav-mobile" class="central hide-on-med-and-down">
        <li>{{ $assay->name }}</li>
      </ul>
      <ul id="nav-switch" class="right hide-on-med-and-down">
        {{-- {{ Form::button('<i class="material-icons">save</i>', ['id'=>'btnSaveChoose', 'class'=>'btn btn-warning']) }} --}}
        <li><a class="waves-effect waves-light btn" id="btnSaveChoose"><i class="material-icons left">save</i>Salvar</a></li>
         <li>|</li>
         <li><a class="waves-effect waves-light btn light-blue darken-4" id="btnConfirmChoose"><i class="material-icons left">send</i>Enviar Ensaio</a></li>
         {{-- <li>{!! Form::submit('Enviar Escolhas', ['id'=>'btnSendChoose', 'class'=>'btn teal waves-light']) !!}</li> --}}

        {{--<li>Usar marca d'água?</li>
        <li class="item-switch">
          <div class="switch" >
            <label>Não<input id="switchLiberar" type="checkbox" {!! ($assay->released) ? "checked" : ""; !!} name='release' value='{{ $assay->id }}'>
              <span class="lever"></span>Sim
            </label>
          </div>
        </li> --}}
      </ul>
    </div>
  </nav>
  <br>
  <ul class="collapsible grey lighten-3" data-collapsible="expandable">
    <li>
      <div class="collapsible-header black-text">
        <i class="material-icons">touch_app</i>Fotos escolhidas: <span id="photos_selected">0</span> de {{ count($photos) }}  disponíveis
      </div>
      <div class="row collapsible-body">
        <div class="col s12">
          <div id="choose-photos" class="card material-table grey-text">
          </div>
        </div>
      </div>
    </li>
  </ul>

  <ul class="collapsible grey lighten-3" data-collapsible="expandable">
    <li>
      <div class="collapsible-header black-text active"><i class="material-icons">photo_album</i>Galeria</div>
      <div class="row collapsible-body">
        <div id="admin" class="col s12">
          <div class="card material-table">
            <div id="ng2" data-nanogallery2>
              @foreach ($photos as $photo)
              {{-- @foreach ($assay->photos as $photo) --}}
                <a data-ngcustomData='{{ $photo->id }}' data-ngid='{{ $photo->id }}' href="{{ asset('storage/images/'.$photo->path) }}" data-ngThumb="{{ asset('storage/images/thumb/'.$photo->path) }}"> {{ $photo->image_name }} </a>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </li>
  </ul>
  <!-- Modal CHAT Structure -->
  <div class="chat">
    <div id="commentsModal" class="modal">
      <div class="modal-content">
        <h4>Comentários</h4>
        <p class="chat-title"></p>
        <div class="comment"></div>
      </div>
      <div class="modal-footer chat-footer">
        <div class="row">
          <div class="col s10 m10 l10">
            <input type="text" id="newComment" placeholder="escreva seu comentário">
          </div>
          <a href="#!" id="sendComment" class="btn waves-effect waves-light"><i class="material-icons">send</i></a>
        </div>
      </div>
    </div>
  </div>
  <!-- END MODAL -->


  {{-- ---------------------- MODAL ---------------------------- --}}
  <div id="modal_confirm_choose" class="modal bottom-sheet">
    <div class="modal-content black-text">
      <h5>Você tem certeza que deseja enviar suas escolhas?</h5>
      <h6>Se você confirmar agora, suas escolhas serão enviadas para o seu fotógrafo e somente poderão ser alteradas caso ele libere.</h6>
      <div class="row">

      </div>

    </div>
    <div class="modal-footer">
      <div class="row">
        <div class="col s4"></div>
        <div class="col s2">
          <a class="waves-effect waves-teal btn-flat close">Voltar</a>
        </div>
        <div class="col s2">
          <a class="waves-effect waves-teal btn-flat" id='btnSendChoose'>Tenho certeza</a>
        </div>
      </div>
    </div>
  </div>
  {{-- -------------------- FIM MODAL -------------------------- --}}
  <script type="text/javascript" src="{{ asset('js/jquery.nanogallery2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/watermark.min.js') }}"></script>

  <script type="text/javascript">

  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('[name="_token"]').val()
    }
  });

    var ImageID = [];

    $("#ng2").nanogallery2({
      galleryDisplayMode: 'moreButton',
      gallerySorting: 'titleAsc',
      galleryTheme: 'light',
      allowHTMLinData: true,
      thumbnailSelectable: true, // allow select thumbnailSelectable
      thumbnailLabel: { position: 'overImageOnBottom', hideIcons: true, display: true, titleMaxLength: 30 },
      // thumbnailToolbarImage: { topLeft: 'select', topRight: 'cart' }, //download, share, cart, info
      thumbnailToolbarImage: { topLeft: 'select', topRight : 'custom1' }, //download, share, cart, info
      viewerToolbar: { standard :'infoButton, linkOriginalButton, label, fullscreenButton'}, //menu de baixo quando abre a imagem
      // viewerDisplayLogo: true,
      fnThumbnailToolCustAction : openComments,
      icons: { thumbnailCustomTool1 : '<i class="fa fa-comment best-align-thumbnail" aria-hidden="true"></i>'}
    });

    function openComments(action, item, id_photo = false){
      if(id_photo != false){
        var id_foto = id_photo;
      }else{
        var id_foto = item.GetID();
      }
      $("#sendComment").attr("id-photo", id_foto);//insere no botão de envior comentarios
      $.ajax({
        type: "POST",
        url: "/studio/comentario/"+id_foto+"/fotos",
        beforeSend: function(){
          var actual = $("#commentsModal .modal-content .comment");
          actual.html("");
        },
        success: function( res ) {
          var n_msg = res.config.n_messages;
          if(n_msg > 0){
            $("#commentsModal .modal-content > p.chat-title").html("FOTO: "+res.config.photo_name);
            for(var i=0; i < n_msg; i++){
              actual = $("#commentsModal .modal-content .comment");
              if(res[i].sender_type == "App\\Client"){
                var newComment = ""+
                "<div class='message-bubble'>"+
                  "<div class='chat-bubble round btm-right right'>"+
                    "<div class='chat-text'>"+
                    "<span class='chat-photographer-photo'>C</span>"+
                    // "<span class='chat-name right-align'>"+res[i].sender.name+"</span>"+
                    "<p>"+res[i].message+"</p>"+
                    "<span class='chat-date right-align'>"+res[i].date+"</span>"+
                    "</div>"+
                  "</div>";
                "</div>";
              }else{
                var newComment = ""+
                  "<div class='message-bubble'>"+
                    "<div class='chat-bubble round btm-left left'>"+
                      "<div class='chat-text'>"+
                      "<span class='chat-client-photo'>P</span>"+
                      "<span class='chat-name left-align'>"+res[i].sender.name+"</span>"+
                      "<p>"+res[i].message+"</p>"+
                      "<span class='chat-date left-align'>"+res[i].date+"</span>"+
                      "</div>"+
                    "</div>"+
                  "</div>";
              }
              actual.append(newComment);
            }
          }
        }
      });
      $('#commentsModal').modal('open');
    }

    $("#sendComment").click(function(){
      var comentario = $("#newComment").val();
      var id_photo = $(this).attr("id-photo");
      if(comentario.length > 0){
        $.ajax({
          type: "POST",
          url: "/studio/comentario/store",
          data: {
            "comment" : comentario,
            "id_photo" : id_photo,
            "sender" : "App\\Client"
          },
          success: function( res ) {
            if(res.status){
              $("#newComment").val("");
              openComments(false, false, id_photo);
            }
          }
        });
      }
    });

    function writePhotoName(e){
      itens = $("#ng2").nanogallery2('itemsSelectedGet');
      $("#photos_selected").html(itens.length);
      $("#choose-photos").html(" ");
      let item;
      ImageID = [];
      for (let i=0; i < itens.length; ++i) {
        ImageID.push(itens[i].customData);
        // console.log("CUSTOM: " + itens[i].customData +" -- IDGAL: "+ itens[i].GetID());
        if(item == undefined){
          item = itens[i].title;
        }else{
          item = item.trim() + ", " + itens[i].title;
        }
        $("#choose-photos").html(item);
      }
    }

    $("#ng2").on( 'itemSelected.nanogallery2', writePhotoName );
    $("#ng2").on( 'itemUnSelected.nanogallery2', writePhotoName );


    function saveChoose(typeChoose){
      assay = {{ $assay->id }};
      $.ajax({
        type: "POST",
        url: "/cliente/saveSelected",
        data: { 'assay_id': assay, 'photos_selected' : ImageID , 'type' : typeChoose},
        success: function( msg ) {
          if(msg.redirect == true){
            window.location.replace("/cliente");
          }
          toastContent = $('<span>'+msg.message+'</span>');
          Toast(toastContent, 2000, 'assay-toast');
        },
        error: function(msg){
          // $("#apagar").html(msg.responseText);
        }
      });
    }


    $("#btnSaveChoose").click(function(){
      saveChoose('save');
    });

    $("#btnSendChoose").click(function(){
      saveChoose('send');
    });


    $(document).ready(function(){
      assay = {{ $assay->id }};
      $.ajax({
        type: "POST",
        url: "/cliente/getSelected",
        data: { 'assay_id': assay},
        success: function( r ) {
          // console.log(r.selectedIDs);
          var items=$('#ng2').nanogallery2('data').items;
          items.forEach( function(curr) {
              if(r.selectedIDs.indexOf(curr.customData) != -1){
                $('#ng2').nanogallery2('itemsSetSelectedValue', [curr], true);
              }
          });
          writePhotoName();
        },
      });
    });

    $("#btnConfirmChoose").click(function(){
      $("#modal_confirm_choose").modal('open');
    })

    $(".close").click(function(){
      $('#modal_confirm_choose').modal('close');
    })

  </script>



  </script>
@endsection
