@extends('client.base')
@section('body')
<div class="body">
  <div class="row center-align">
    <div class="nav-wrapper col s12 valign-wrapper left-align">
      @if( url()->current() != (url('').'/cliente'))
      <div class="col s1 valign-wrapper left-align">
        <i class="clickable material-icons prefix link-back" href="{{ route('cliente') }}" >arrow_back</i>
      </div>
      @endif
      <div class="col s9">
        @if(Auth::guard('web_client')->user()->name)
          <span class="flow-text bolder">{{ Auth::guard('web_client')->user()->name }}</span>
        @endif
      </div>
      <div class="col s2 m5 l5 right-align config-icons">
        {{-- <i class="clickable material-icons prefix tooltipped" id="search-ico" data-position="bottom" data-delay="50" data-tooltip="Buscar">search</i>
        <i class="clickable material-icons prefix tooltipped" id="comment-ico"data-position="bottom" data-delay="50" data-tooltip="Mensagens">comment</i> --}}
        <a class="dropdown-button" href="#!" data-activates="dropdownConfig"><i class="clickable material-icons prefix tooltipped" id="config-ico" data-position="bottom" data-delay="50" data-tooltip="Configuração">more_vert</i></a>
      </div>
        <ul id="dropdownConfig" class="dropdown-content">
          {{-- <li><a href="{{ route('user.profile') }}"><i class="clickable material-icons prefix tooltipped tiny" data-position="bottom" data-delay="50" data-tooltip="Configuração">perm_identity</i>Perfil</a></li> --}}
          <li><a href="{{ route('user.logout') }}" id="logOutBtn"><i class="clickable material-icons prefix tooltipped tiny" id="config-ico" data-position="bottom" data-delay="50" data-tooltip="Configuração">input</i>Sair</a></li>
        </ul>

    </div>
  </div>
</div>
<div class="row center-align">
  <div class="change-content">
    <div class="col s12 m12 l12" id="contenthere">
      @yield('content')
    </div>
  </div>
</div>
<!-- Modal Structure -->
<div id="modalMensagem" class="modal bottom-sheet">
  <div class="modal-content" id="modalMensagemConteudo">
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Ok</a>
  </div>
</div>
<script type="text/javascript">

</script>
@endsection
