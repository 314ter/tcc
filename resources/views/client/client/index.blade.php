@extends('studio.studio')

@section('content')

  <div class="row">
  <div id="admin" class="col s12">
    <div class="card material-table">
      <div class="table-header">
        <span class="table-title">Seus clientes cadastrados</span>
        <div class="actions">
          <a href="{{ route('cliente.create') }}" class="modal-trigger waves-effect btn-flat nopadding"><i class="material-icons">person_add</i></a>
          <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
        </div>
      </div>
      <table id="datatable">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Data de Nascimento</th>
            <th>Telefone</th>
            <th>Email</th>
            {{-- <th>Ult. Login</th> --}}
          </tr>
        </thead>
        <tbody>
          @foreach($clients as $client)
              <tr class="editLink" id="{{ $client->id }}">
                <td>{{ $client->name }}</td>
                <td>{{ $client->birthdate }}</td>
                <td>{{ $client->phone }}</td>
                <td>{{ $client->email }}</td>
              </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
