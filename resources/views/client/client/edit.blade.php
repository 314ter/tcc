@extends('studio.studio')

@section('content')
  <div class="container">
    {{-- {!! Form::model($client, ['method'=>'PATCH', 'action'=>['ClientController@update', $client->id]]) !!} --}}
    {!! Form::model($client, ['route'=>['cliente.update', $client->id]]) !!}
    <?php // TODO: ENVIAR ID DO FOTOGRAFO JUNTO ?>
      <div class="section white">
        <div class="container">
        <div class="row" id="pre_register">
          <div class="col s12">
            <h5 class="grey-text">Edição de Clientes</h5>
              <div class="row">
                  {{-- {!! Form::text('register_token', old('register_token'), ['placeholder'=>'cole o código aqui', 'class'=>'register_token_field']) !!} --}}
                <div class="input-field col s12">
                  {!! Form::text('name') !!}
                  {!! Form::label('name', 'Nome Completo:') !!}
                </div>
                <div class="col s4"></div>
              </div>
              <div class="row">
                <div class="input-field col s6">
                  <p>
                    {!! Form::checkbox('juridical', 'yes', false, ['id'=>'juridical']) !!}
                    {!! Form::label('juridical', 'Pessoa Jurídica?', ['id'=>'juridicalLabel']) !!}
                  </p>
                </div>
                <div id="divCPF" class="input-field col s6">
                  {!! Form::text('cpf', null) !!}
                  {!! Form::label('cpf', 'CPF:') !!}
                </div>
                <div id="divCNPJ" class="input-field col s6 hide">
                  {!! Form::text('cnpj', null) !!}
                  {!! Form::label('cnpj', 'CNPJ:') !!}
                </div>
              </div>
              <div class="row">
                <div class="input-field col s4">
                  {!! Form::text('postal_code', $address->postal_code, ['id'=>'postal_code']) !!}
                  {!! Form::label('postal_code', 'CEP:') !!}
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  {!! Form::text('address_name', $address->address_name) !!}
                  {!! Form::label('address_name', 'Endereço:') !!}
                </div>
              </div>
              <div class="row">
                <div class="input-field col s6">
                  {!! Form::text('district', $address->district) !!}
                  {!! Form::label('district', 'Bairro:') !!}
                </div>
                <div class="input-field col s6">
                  {!! Form::text('city', $address->city) !!}
                  {!! Form::label('city', 'Cidade:') !!}
                </div>
              </div>
              <div class="row">
                <div class="input-field col s6">
                  {!! Form::select('state', [
                    'AC'=>'Acre',
                    'AL'=>'Alagoas',
                    'AP'=>'Amapá',
                    'AM'=>'Amazonas',
                    'BA'=>'Bahia',
                    'CE'=>'Ceará',
                    'DF'=>'Distrito Federal',
                    'ES'=>'Espírito Santo',
                    'GO'=>'Goiás',
                    'MA'=>'Maranhão',
                    'MT'=>'Mato Grosso',
                    'MS'=>'Mato Grosso do Sul',
                    'MG'=>'Minas Gerais',
                    'PA'=>'Pará',
                    'PB'=>'Paraíba',
                    'PR'=>'Paraná',
                    'PE'=>'Pernambuco',
                    'PE'=>'Piauí',
                    'RJ'=>'Rio de Janeiro',
                    'RN'=>'Rio Grande do Norte',
                    'RS'=>'Rio Grande do Sul',
                    'RO'=>'Rondônia',
                    'RR'=>'Roraima',
                    'SC'=>'Santa Catarina',
                    'SP'=>'São Paulo',
                    'SE'=>'Sergipe',
                    'TO'=>'Tocantins'
                  ], $address->state); !!}

                  {!! Form::label('district', 'Estado:') !!}
                </div>
                <div class="input-field col s6">
                  {!! Form::text('country', $address->country, ['maxlength'=>2]) !!}
                  {!! Form::label('country', 'País:') !!}
                </div>
                <?php // TODO: LOCALIZAÇãO ?>
              </div>
              <div class="row">
                <div class="input-field col s4">
                  {!! Form::select('marital_status', [
                    'SOLTEIRO'=>'Solteiro',
                    'CASADO'=>'Casado',
                    'SEPARADO'=>'Separado',
                    'DIVORCIADO'=>'Divorciado',
                    'VIÚVO'=>'Viúvo'
                  ], $client->marital_status); !!}
                  {!! Form::label('marital_status', 'Estado Civil:') !!}
                </div>
                <div class="input-field col s4">
                  {!! Form::select('gender', [
                    'F'=>'Feminino',
                    'M'=>'Masculino'
                  ]); !!}
                  {!! Form::label('gender', 'Sexo:') !!}
                </div>
                <div class="input-field col s4">
                  {!! Form::label('birthdate', 'Data de Nascimento:') !!}
                  {!! Form::text('birthdate', null, ['class'=>'datepicker']) !!}
                </div>
              </div>
              <div class="row">
                <div class="input-field col s4">
                  {!! Form::text('phone', null, ['placeholder'=>'(99)99999-9999']) !!}
                  {!! Form::label('phone', 'Telefone:') !!}
                </div>
                <div class="input-field col s8">
                  {!! Form::text('email',null) !!}
                  {!! Form::label('email', 'E-mail:') !!}
                </div>
              </div>
              <div class="row">

                <div class="input-field col s4">
                </div>
              </div>
              <div class="row">
                <div class="col s12 center">
                  {!! Form::submit('Editar', ['class'=>'btn-large waves-effect waves-light']) !!}
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
    {!! Form::close() !!}
  </div>

  <script type="text/javascript">
    $(document).ready(function(){
      if($("[name='cpf']").val() == ""){
        $("#divCPF").toggleClass('hide');
        $("#divCNPJ").toggleClass('hide');
        $("#juridical").prop("checked", "checked");
      }
    })
  </script>
@endsection
