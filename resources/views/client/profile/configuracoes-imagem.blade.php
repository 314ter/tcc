@extends('studio.profile.index')

@section('profile-content')
  <link rel="stylesheet" href="{{ asset('css/dropzone.css') }}">
  <style media="screen">
    #op1{
      background-image: url('{{ asset('images/opacitytest/op1.jpg') }}');
      background-position: 0 0;
      background-repeat: no-repeat;
      background-size: cover;
      position: relative;
      width: 500px;
      height: 500px;
    }
    #op2{
      background-image: url('{{ asset('images/opacitytest/op2.jpg') }}');
      background-position: 0 0;
      background-repeat: no-repeat;
      background-size: cover;
      position: relative;
      width: 500px;
      height: 500px;
    }
    #op3{
      background-image: url('{{ asset('images/opacitytest/op3.jpg') }}');
      background-position: 0 0;
      background-repeat: no-repeat;
      background-size: cover;
      position: relative;
      width: 500px;
      height: 500px;
    }
    .op{
    }
    .op img{
      position: relative;
      top: 50%;
      transform: translateY(-50%);
    }
  </style>
  <div class="row">
    <div class="col s12 m12 l12">
      <h3>Configurações de Imagens</h3>
    </div>
  </div>

  <div class="row">
    <div class="col s12 m12 l12">
      <ul class="collapsible" data-collapsible="expandable">
        <li>
          <div class="collapsible-header black-text active"><i class="material-icons">add_a_photo</i> Marca D'água</div>
          <div class="row collapsible-body">
            <div class="col s12 m12 l12 grey-text lighten-1">
              {!! Form::open(['action' => 'MaskController@store', 'files'=> true, 'class' => 'dropzone uploader grey lighten-4', 'id'=>'dropzoneUploader']) !!}
              {{-- {!! Form::model($mask, ['route'=>['uploadImageMask', $mask->id],'files'=> true, 'class' => 'dropzone uploader grey lighten-4', 'id'=>'dropzoneUploader']) !!} --}}
              {{-- {!! Form::hidden('mask_id', $mask->id) !!} --}}
              {{-- {!! Form::submit('Editar', ['class'=>'btn-large waves-effect waves-light']) !!} --}}
              <i class="material-icons medium grey-text lighten-1">backup</i>
              <h6 class="grey-text lighten-1">Arraste sua marca para cá.</h6>
              <h6 class="grey-text lighten-1">- ou -</h6>
              <div class="teal-text lighten-2">
                <span>clique aqui para abrir o navegador de arquivos.</span>
                {{-- <input type="file" name="file" multiple> --}}
              </div>
              {!! Form::close() !!}
            </div>
            <br><br>
            <div class="image-opacity-test hide">
              <div class="row range-opacity">
                <div class="col s3 m3 l3"></div>
                <div class="col s6 m6 l6">
                  Controle de Opacidade:
                  <p class="range-field">
                    <input type="range" id="opacityRange" min="0" max="100" value="100"/>
                  </p>
                </div>
              </div>
              {{-- <div class="row range-size">
                <div class="col s3 m3 l3"></div>
                <div class="col s6 m6 l6">
                  Controle de Tamanho:
                  <p class="range-field">
                    <input type="range" id="sizeRange" min="0" max="100" value="100"/>
                  </p>
                </div>
                TODO: Controle de posição da imagem
              </div> --}}
              <div class="row">
                {!! Form::submit('Salvar', ['class'=>'btn waves-effect waves-light', 'id'=>'updateMask']) !!}
              </div>
              <div class="center">
                <div class="col s4 m4 l4 op" id="op1"></div>
                <div class="col s4 m4 l4 op" id="op2"></div>
                <div class="col s4 m4 l4 op" id="op3"></div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>


  <script type="text/javascript" src="{{ asset('js/dropzone.js') }}"></script>
  <script type="text/javascript">
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('[name="_token"]').val()
      }
    });

    Dropzone.options.dropzoneUploader = {
      paramName: "file", // The name that will be used to transfer the file
      maxFilesize: 8, // MB,
      acceptedFiles: 'image/*',
      maxFiles: 1, // only 1 image
      // params: { 'id' : $("input[name='mask_id']").val() }, // parametro a ser enviado para o servidor
      addRemoveLinks: true,
      success: function(file, response){
        changeMaskPath(response.path);
        $('.image-opacity-test').removeClass('hide');
      },
      removedfile: function(file) {
        var name = file.name;
        if (name) {
          $.ajax({
            type: 'POST',
            url: "/removeImageAssay/"+name,  //passes the image name to  the method handling this url to //remove file
            dataType: 'json'
          });
        }
          var _ref;
          return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
      },
    };

    function changeMaskPath(path){
      realPath = "http://" + window.location.host + "/" + path;
      $('.op').html(" ");
      $('.op').html("<img class='responsive-img opimg' src='"+realPath+"'>");
    }

    $('#opacityRange').on('input', function () {
      range = parseFloat($(this).val());
      $('.opimg').animate({
        opacity: range/100
      }, 10);
        console.log(typeof(range));
    });

    $("#updateMask").click(function(){
      opacity = parseFloat($('#opacityRange').val())/100;
      // _token = $('[name="_token"]').val();
      $.ajax({
        type: "POST",
        url: "/uploadConfMask",
        data: { 'opacity': opacity },

        success: function( msg ) {
          if(msg.success){
            toastContent = $('<span>'+msg.message+'</span>');
            Toast(toastContent, 2000);
          }else{
            toastContent = $('<span>Não foi possível salvar sua configuração!</span>');
            Toast(toastContent, 2000);
          }

        },
        error: function(msg){
          toastContent = $('<span>Não foi possível salvar o tipo!</span>');
          Toast(toastContent, 2000);
        },
      });
    });
  </script>

@endsection
