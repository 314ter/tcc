@extends('client.studio')
@section('content')
  <div class="container">
    <div class="white">
      <ul class="collection left-align">
        @if(count($assays) > 0)
          @foreach ($assays as $assay)
            <li class="collection-item avatar" id="{{ $assay->id }}">
              <img src="images/yuna.jpg" alt="" class="circle">
              <span class="title grey-text"> {{ $assay->name}}</span>
              <p><strong>Fotógraf@: </strong> {{ $assay->contract->user->name }}</p>
              {{-- <p><strong>Ensaio:</strong> </p> --}}
              <p><strong>Data final para escolha:</strong> {{ $assay->final_date }}</p>
              {{-- <p class="teal-text right-align small"><strong>{{ count($assay->pick()->toSql()) }} fotos escolhidas de {{ count($assay->photos()->get()) }} disponíveis.</strong></p> --}}
              <p class="teal-text right-align small"><strong>{{ count($assay->pick()->get()) }} fotos escolhidas de {{ count($assay->photos()->get()) }} disponíveis.</strong></p>
              <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
            </li>
          @endforeach
        @else
          <div class="row">
            <div class="col s12 m12 l12">
              <h5 class="black-text center">Você não possui nenhuma seleção de fotos disponível!</h5>
            </div>
          </div>
        @endif
      </ul>
    </div>

  </div>

  <script type="text/javascript">
    $("li.collection-item").click(function(){
      url = window.location.href;
      window.location.replace(url+"/"+$(this).prop('id'));
    });

    $(document).ready(function(){
      mensagem = '{{ session('error') }}';

      if(mensagem.length > 0){
        toastContent = $('<span>'+mensagem+'</span>');
        Toast(toastContent, 5000);
      }
    })
  </script>
@endsection
