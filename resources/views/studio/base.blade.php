<!DOCTYPE html>
<html lang="pt">
<head>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>[Studio]</title>
  <link rel="icon" href="images/favicon.png" type="image/x-icon"/>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  {!! MaterializeCSS::include_full() !!}
  <link type="text/css" rel="stylesheet" href="{{ asset('css/studio.css') }}">
  <link type="text/css" rel="stylesheet" href="{{ asset('css/dataTables.css') }}">
</head>
<body>
  @yield('body')
  @yield('footer')
  <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('js/dataTables.js')}}"></script>
  <script src="{{asset('js/studio.js')}}"></script>
  <script src="{{asset('js/init.js')}}"></script>

</body>
</html>
