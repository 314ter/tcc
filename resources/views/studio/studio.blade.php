@extends('studio.base')
@section('body')
<div class="body">
  <div class="row center-align">
    <div class="nav-wrapper col s12 valign-wrapper left-align">
      <div class="col s1 valign-wrapper left-align">
        <i class="clickable material-icons prefix" id="menucollapse">menu</i>
      </div>
      <div class="col s9">
        @if(Auth::user()->name)
          <span class="flow-text bolder">{{ Auth::user()->name }}</span>
        @else
          <span class="flow-text bolder">[:o:] Photobooker</span>
        @endif
      </div>
      <div class="col s2 m5 l5 right-align config-icons">
        {{-- <i class="clickable material-icons prefix tooltipped" id="search-ico" data-position="bottom" data-delay="50" data-tooltip="Buscar">search</i> --}}
        <i class="clickable material-icons prefix tooltipped" id="comment-ico"data-position="bottom" data-delay="50" data-tooltip="Mensagens">comment</i>
        <a class="dropdown-button" href="#!" data-activates="dropdownConfig"><i class="clickable material-icons prefix tooltipped" id="config-ico" data-position="bottom" data-delay="50" data-tooltip="Configuração">more_vert</i></a>
      </div>
        <ul id="dropdownConfig" class="dropdown-content">
          <li><a href="{{ route('user.profile') }}"><i class="clickable material-icons prefix tooltipped tiny" data-position="bottom" data-delay="50" data-tooltip="Configuração">perm_identity</i>Perfil</a></li>
          <li><a href="{{ route('user.logout') }}" id="logOutBtn"><i class="clickable material-icons prefix tooltipped tiny" id="config-ico" data-position="bottom" data-delay="50" data-tooltip="Configuração">input</i>Sair</a></li>
        </ul>

    </div>
    <div class="sidenav-wrapper {!! \Request::is('studio/*') ? 'on' : '' !!}" id="slideout">
      <div class="row center">
        <a href="{!! url('/studio') !!}"><img src="{{ asset('images/LOGO.png') }}" class="foto-logo" alt="Photobooker"></a>
      </div>
      <div class="row">
        <div class="btn-side-menu col s12">
          <a href="{{ route('cliente.index') }}" class="grey darken-4 btn-large tooltipped falsebutton" data-position="right" data-delay="50" data-tooltip="Clientes"><img src="{{ asset('images/users.svg') }}" class="ico-btn left" alt="Clientes"><span class="hide-on-med-and-down">Clientes</span></a>
        </div>
      </div>
      <div class="row">
        <div class="btn-side-menu col s12">
          <a href="{{ route('contrato.index') }}" class="grey darken-4 btn-large tooltipped falsebutton" data-position="right" data-delay="50" data-tooltip="Contratos"><img src="{{ asset('images/contract.svg') }}" class="ico-btn left" alt="Contratos"><span class="hide-on-med-and-down">Contratos</span></a>
        </div>
      </div>
      <div class="row">
        <div class="btn-side-menu col s12">
          <a href="{{ route('ensaio.index') }}" class="grey darken-4 btn-large tooltipped falsebutton" data-position="right" data-delay="50" data-tooltip="Ensaios"><img src={{ asset('images/photo-camera.svg') }} class="ico-btn left" alt="Ensaios"><span class="hide-on-med-and-down">Ensaios</span></a>
        </div>
      </div>
      {{-- <div class="row">
        <div class="btn-side-menu col s12">
          <a href="{{ route('cliente.index') }}" class="grey darken-4 btn-large tooltipped falsebutton" data-position="right" data-delay="50" data-tooltip="Finanças"><img src="{{ asset('images/piggy-bank.svg') }}" class="ico-btn left" alt="Finanças"><span class="hide-on-med-and-down">Finanças</span></a>
        </div>
      </div> --}}
      {{-- <div class="row">
        <div class="btn-side-menu col s12">
          <a href="{{ route('cliente.index') }}" class="grey darken-4 btn-large tooltipped falsebutton" data-position="right" data-delay="50" data-tooltip="Agenda"><img src="{{ asset('images/small-calendar.svg') }}" class="ico-btn left" alt="Agenda"><span class="hide-on-med-and-down">Agenda</span></a>
        </div>
      </div> --}}
    </div>
  </div>
</div>
<div class="row center-align">
  <div class="change-content">
    <div class="col s12 m12 l12" id="contenthere">
      @yield('content')
    </div>
  </div>
</div>
<!-- Modal Structure -->
<div id="modalMensagem" class="modal bottom-sheet">
  <div class="modal-content" id="modalMensagemConteudo">
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Ok</a>
  </div>
</div>


  <div class="fixed-action-btn toolbar alert-mew-message hide">
    <a class="btn-floating btn-large red new-message">
      <i class="material-icons">insert_comment</i>
    </a>
    <ul>
      <div class="col s5 m5 l5 message-modal">
        <span class="right">Você tem uma nova mensagem de: <span id="client-message"></span></span>
      </div>
      <div class="col s5 m5 l5 message-modal">
        <span class="client-message left"><strong>" <span id="message-message"></span>"</strong></span>
      </div>
      <li class="waves-effect waves-light"><a id="link-readed" href="javascript:;">marcar como lido<i class="material-icons">markunread</i></a></li>
      <li class="waves-effect waves-light"><a id="link-photo" href="#!"><i class="material-icons">photo_library</i>ir para a foto</a></li>
    </ul>
  </div>
@endsection
