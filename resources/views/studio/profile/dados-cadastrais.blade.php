@extends('studio.profile.index')

@section('profile-content')
  <div class="row">
    <div class="col s12 m12 l12">
      <h3>Dados Cadastrais</h3>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      {!! Form::text('name', $user->name) !!}
      {!! Form::label('name', 'Nome Completo:') !!}
    </div>
    <div class="col s4"></div>
  </div>
  <div class="row">
    <div class="input-field col s6">
      <p>
        {!! Form::checkbox('juridical', 'yes', false, ['id'=>'juridical']) !!}
        {!! Form::label('juridical', 'Pessoa Jurídica?', ['id'=>'juridicalLabel']) !!}
      </p>
    </div>
    <div id="divCPF" class="input-field col s6">
      {!! Form::text('cpf', $user->cpf) !!}
      {!! Form::label('cpf', 'CPF:') !!}
    </div>
    <div id="divCNPJ" class="input-field col s6 hide">
      {!! Form::text('cnpj', $user->cnpj) !!}
      {!! Form::label('cnpj', 'CNPJ:') !!}
    </div>
  </div>
  <div class="row">
    <div class="input-field col s4">
      {!! Form::text('postal_code', $address->cep, ['id'=>'postal_code']) !!}
      {!! Form::label('postal_code', 'CEP:') !!}
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      {!! Form::text('address_name', $address->address_name) !!}
      {!! Form::label('address_name', 'Endereço:') !!}
    </div>
  </div>
  <div class="row">
    <div class="input-field col s6">
      {!! Form::text('district', $address->district) !!}
      {!! Form::label('district', 'Bairro:') !!}
    </div>
    <div class="input-field col s6">
      {!! Form::text('city', $address->city) !!}
      {!! Form::label('city', 'Cidade:') !!}
    </div>
  </div>
  <div class="row">
    <div class="input-field col s6">
      {!! Form::select('state', [
        'AC'=>'Acre',
        'AL'=>'Alagoas',
        'AP'=>'Amapá',
        'AM'=>'Amazonas',
        'BA'=>'Bahia',
        'CE'=>'Ceará',
        'DF'=>'Distrito Federal',
        'ES'=>'Espírito Santo',
        'GO'=>'Goiás',
        'MA'=>'Maranhão',
        'MT'=>'Mato Grosso',
        'MS'=>'Mato Grosso do Sul',
        'MG'=>'Minas Gerais',
        'PA'=>'Pará',
        'PB'=>'Paraíba',
        'PR'=>'Paraná',
        'PE'=>'Pernambuco',
        'PE'=>'Piauí',
        'RJ'=>'Rio de Janeiro',
        'RN'=>'Rio Grande do Norte',
        'RS'=>'Rio Grande do Sul',
        'RO'=>'Rondônia',
        'RR'=>'Roraima',
        'SC'=>'Santa Catarina',
        'SP'=>'São Paulo',
        'SE'=>'Sergipe',
        'TO'=>'Tocantins'
      ], $address->state); !!}
      {!! Form::label('state', 'Estado:') !!}
    </div>
    <div class="input-field col s6">
      {!! Form::text('country', $address->country, ['maxlength'=>2]) !!}
      {!! Form::label('country', 'País:') !!}
    </div>
    <?php // TODO: LOCALIZAÇãO ?>
  </div>
  <div class="row">
    <div class="input-field col s4">
      {!! Form::label('birthdate', 'Data de Nascimento:') !!}
      {!! Form::text('birthdate', $user->birthdate, ['class'=>'datepicker']) !!}
    </div>
    <div class="input-field col s4">
      {!! Form::text('phone', $user->phone, ['placeholder'=>'(99)99999-9999']) !!}
      {!! Form::label('phone', 'Telefone:') !!}
    </div>
  </div>
  <div class="row">
    <div class="input-field col s6">
      {!! Form::text('email', $user['email'], ['disabled']) !!}
      {!! Form::label('email', 'E-mail:') !!}
    </div>
    <div class="input-field col s4">
      {{-- {!! Form::password('password', null) !!}
      {!! Form::label('password', 'Senha:') !!} --}}
    </div>
  </div>
  <div class="row">
    <div class="col s12 center">
      <a id="btn-register-user" class="btn-large waves-effect waves-light">Alterar Dados <i class="right  material-icons">done</i></a>
    </div>
  </div>
</div>
@endsection
