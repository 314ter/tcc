@extends('studio.studio')
@section('content')
  <div class="grey lighten-4 black-text">
    <div class="row">
      <div class="col s3 m3 l3 left-controll">
        <div class="container section scrollspy">
          <ul class="left-align">
            <li><a href="{{ route('user.profile') }}"><i class="material-icons">assignment_ind</i>Dados Cadastrais</a></li>
            <li><a href="{{ route('user.image_configuration') }}"><i class="material-icons">camera_enhance</i>Configurações de Imagem</a></li>
          </ul>
        </div>
      </div>
      <div class="col s9 m9 l9 right-content">
        <div class="container">
          @yield('profile-content')
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $(window).scroll(function(){
      if($(this).scrollTop() > 111){
        $('.left-controll').addClass('top-fixed');
      }else{
        $('.left-controll').removeClass('top-fixed');
      }
    })
  </script>
@endsection
