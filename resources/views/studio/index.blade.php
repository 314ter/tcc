@extends('studio.studio')

@section('content')
  <link href="{!! asset('css/chart/style.css') !!}" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="{!! asset('css/chart/custom-style.css') !!}" type="text/css" rel="stylesheet" media="screen,projection">

  <div id="chartjs" class="section grey lighten-3">
    <div class="row">
      <div class="col s10 m7 l8">
        <h4 class="header grey-text lighten-1">Ensaios</h4>
        <div class="sample-chart-wrapper">
          <canvas id="line-chart-sample" height="90"></canvas>
        </div>
      </div>
      <div class="col s2 m5 l4">
        <div class="invoice-lable">
          <div class="col s12 m12 l12 cyan">
            <h6 class="white-text">Últimas Fotos Selecionadas</h6>
          </div>
        </div>
        <div class="invoice-table">
          <div class="row">
            <div class="col s12 m12 l12">
              <table class="striped grey-text lighten-1">
                <thead>
                  <tr>
                    <th data-field="item">Ensaio</th>
                    <th data-field="price">Cliente</th>
                    <th data-field="uprice">Data Escolha</th>
                    <th data-field="price">Qtd</th>
                  </tr>
                </thead>
                <tbody id="tbody_last_photos">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
        <div class="col s5 m5 l5">
          <div class="invoice-lable">
            <div class="col s12 m12 l12 cyan">
              <h6 class="white-text">Últimas Mensagens</h6>
            </div>
          </div>
          <div class="invoice-table">
            <div class="row">
              <div class="col s12 m12 l12">
                <table class="striped grey-text lighten-1">
                  <thead>
                    <tr>
                      <th data-field="item">#</th>
                      <th data-field="price">Cliente</th>
                      <th data-field="uprice">Últ. Mensagem</th>
                      <th data-field="price">Foto</th>
                    </tr>
                  </thead>
                  <tbody id="tbody_last_messages">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      <div class="col s7 m7 l7">
        <div class="invoice-lable">
          <div class="col s12 m12 l12 cyan">
            <h6 class="white-text">Próximos Eventos</h6>
          </div>
        </div>
        <div class="invoice-table">
          <div class="row">
            <div class="col s12 m12 l12">
              <table class="striped grey-text lighten-1">
                <thead>
                  <tr>
                    <th data-field="item">Dia</th>
                    {{-- <th data-field="price">Local</th> --}}
                    <th data-field="uprice">Cliente</th>
                    <th data-field="price">Telefone</th>
                    <th data-field="price">E-mail</th>
                  </tr>
                </thead>
                <tbody id="tbody_last_events">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- @if($errors->any())
    {!! Form::text('error_message', $errors->all, ['id', 'error_message']) !!}
  @endif --}}


  {{-- <div class="divider"></div>
  <!--Morris Donut Chart-->
  <div id="morris-donut-chart" class="section">
    <h4 class="header">Donut Chart</h4>
    <div class="row">
      <div class="col s12 m4 l3">
        <p>Doughnut charts are probably the most commonly used chart there are. They are divided into segments, the arc of each segment shows the proportional value of each piece of data.</p>
      </div>
      <div class="col s12 m8 l9">
        <div class="sample-chart-wrapper">
          <div id="morris-donut" class="graph"></div>
        </div>
      </div>
    </div>
  </div> --}}

  <script type="text/javascript">
  $(document).ready(function(){
    mensagem = '{{ session('error') }}';

    console.log(mensagem);

    if(mensagem.length > 0){
      toastContent = $('<span>'+mensagem+'</span>');
      Toast(toastContent, 5000);
    }
  })
  </script>



{{-- <script type="text/javascript" src="{{asset('js/plugins/morris.min.js')}}"></script> --}}
{{-- <script type="text/javascript" src="{{asset('js/raphael-min.js')}}"></script> --}}

<script type="text/javascript" src="{!! asset('js/chart/chart.min.js') !!}"></script>
{{-- <script type="text/javascript" src="{!! asset('js/chart/chartjs-sample-chart.js') !!}"></script> --}}
@endsection
