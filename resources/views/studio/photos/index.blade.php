@extends('studio.studio')

@section('content')
  <link rel="stylesheet" href="{{ asset('css/dropzone.css') }}">
  <link rel="stylesheet" href="{{ asset('css/chat.css') }}">
  <link rel="stylesheet" href="{{ asset('css/nanogallery2.min.css')}}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
  <nav>
    <div class="nav-wrapper">
      <ul id="nav-mobile" class="central hide-on-med-and-down">
        <li>{{ $assay->name }}</li>
      </ul>
      <ul id="nav-switch" class="right hide-on-med-and-down">
        <li>Liberar Ensaio?</li>
        <li class="item-switch">
          <div class="switch" >
            <label>Não<input id="switchLiberar" type="checkbox" {!! ($assay->released) ? "checked" : ""; !!} name='release' value='{{ $assay->id }}'>
              <span class="lever"></span>Sim
            </label>
          </div>
        </li>
        {{-- <li>|</li>
        <li>Usar marca d'água?</li>
        <li class="item-switch">
          <div class="switch" >
            <label>Não<input id="switchLiberar" type="checkbox" {!! ($assay->released) ? "checked" : ""; !!} name='release' value='{{ $assay->id }}'>
              <span class="lever"></span>Sim
            </label>
          </div>
        </li> --}}
      </ul>
    </div>
  </nav>
  <br>


<!-- Modal Structure -->
<div class="chat">
  <div id="commentsModal" class="modal">
    <div class="modal-content">
      <h4>Comentários</h4>
      <p class="chat-title"></p>
      <div class="comment"></div>
    </div>
    <div class="modal-footer chat-footer">
      <div class="row">
        <div class="col s10 m10 l10">
          <input type="text" id="newComment" placeholder="escreva seu comentário">
        </div>
        <a href="#!" id="sendComment" class="btn waves-effect waves-light"><i class="material-icons">send</i></a>
      </div>
    </div>
  </div>
</div>
<!-- END MODAL -->


  <ul class="collapsible grey lighten-3" data-collapsible="expandable">
    @if(strlen($list) > 0)
      <li>
        <div class="collapsible-header black-text active"><i class="material-icons">photo_size_select_large</i>Fotos Selecionadas</div>
        <div class="row collapsible-body">
          <div class="col s12 m12 l12 grey-text lighten-1">
            {!! Form::text("selectables", $list, ['readonly'=>'readonly']) !!},
          </div>
        </div>
      </li>
    @endif
    <li>
      <div class="collapsible-header black-text active"><i class="material-icons">add_a_photo</i>Carregar Fotos</div>
      <div class="row collapsible-body">
        <div class="col s12 m12 l12 grey-text lighten-1">
          {!! Form::model($assay, ['route'=>['uploadImageAssay', $assay->id],'files'=> true, 'class' => 'dropzone uploader grey lighten-4', 'id'=>'dropzoneUploader']) !!}
          {!! Form::hidden('ensaio_id', $assay->id) !!}
          {{-- {!! Form::submit('Editar', ['class'=>'btn-large waves-effect waves-light']) !!} --}}
          <i class="material-icons medium grey-text lighten-1">backup</i><br>
          <span class="grey-text lighten-1">Arraste suas fotos para cá.</span><br>
          <span class="grey-text lighten-1">- ou -</span><br>
          <span class="teal-text lighten-2">clique aqui para abrir o navegador de arquivos.</span>
          {!! Form::close() !!}
        </div>
      </div>
    </li>
    <li>
      <div class="collapsible-header black-text active"><i class="material-icons">photo_album</i>Galeria</div>
      <div class="row collapsible-body">
        <div id="admin" class="col s12">
          <div class="card material-table">
            @if(count($photos) == 0)
              <div id="ng2EmptyMessage">
                <span class='grey-text lighten-4'>Nenhuma foto carregada até o momento!</span>
              </div>
              <div class="hide" id="ng2" data-nanogallery2>
            @else
              <div id="ng2" data-nanogallery2>
            @endif
            @foreach ($photos as $photo)
              <a data-ngcustomData='{{ $photo->id }}' data-ngid='{{ $photo->id }}' href="{{ asset('storage/images/'.$photo->path) }}" data-ngThumb="{{ asset('storage/images/thumb/'.$photo->path) }}"> {{ $photo->image_name }} </a>
            @endforeach
        </div>
          </div>
        </div>
      </div>
    </li>
</ul>
  <script type="text/javascript" src="{{ asset('js/dropzone.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/jquery.nanogallery2.min.js') }}"></script>
  <script type="text/javascript">
  //$('#yourElement').nanoGallery('reload');

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('[name="_token"]').val()
      }
    });
    function Toast(msg, speed=2000, classe){
      Materialize.toast(msg, speed, classe);
    }

    $("#ng2").nanogallery2({
      galleryDisplayMode: 'moreButton',
      gallerySorting: 'titleAsc',
      galleryTheme: 'light',
      thumbnailSelectable: true, // allow select thumbnailSelectable
      thumbnailLabel: { position: 'overImageOnBottom', hideIcons: true, display: true, titleMaxLength: 30 },
      thumbnailToolbarImage: { topLeft: 'select', topRight : 'custom1' }, //download, share, cart, info
      viewerToolbar: { standard :'minimizeButton, infoButton, downloadButton, linkOriginalButton, label, fullscreenButton'}, //menu de baixo quando abre a imagem
      fnThumbnailToolCustAction : openComments,
      icons: { thumbnailCustomTool1 : '<i class="fa fa-comment best-align-thumbnail" aria-hidden="true"></i>'}
    });

    $("#sendComment").click(function(){
      var comentario = $("#newComment").val();
      var id_photo = $(this).attr("id-photo");
      if(comentario.length > 0){
        $.ajax({
          type: "POST",
          url: "/studio/comentario/store",
          data: {
            "comment" : comentario,
            "id_photo" : id_photo,
            "sender" : "App\\User"
          },
          success: function( res ) {
            if(res.status){
              $("#newComment").val("");
              openComments(false, false, id_photo);
            }
          }
        });
      }
    });

    function openComments(action, item, id_photo = false){
      var id_foto;
      if(id_photo != false){
        id_foto = id_photo;
      }else{
        id_foto = item.GetID();
      }

      console.log(id_foto);
      $("#sendComment").attr("id-photo", id_foto);//insere no botão de envior comentarios
      $.ajax({
        type: "POST",
        url: "/studio/comentario/"+id_foto+"/fotos",
        beforeSend: function(){
          var actual = $("#commentsModal .modal-content .comment");
          actual.html("");
        },
        success: function( res ) {
          var n_msg = res.config.n_messages;
          if(n_msg > 0){
            $("#commentsModal .modal-content > p.chat-title").html("FOTO: "+res.config.photo_name);
            for(var i=0; i < n_msg; i++){
              actual = $("#commentsModal .modal-content .comment");
              if(res[i].sender_type == "App\\Client"){
                var newComment = ""+
                "<div class='message-bubble'>"+
                  "<div class='chat-bubble round btm-left left'>"+
                    "<div class='chat-text'>"+
                    "<span class='chat-client-photo'>C</span>"+
                    // "<span class='chat-name left-align'>"+res[i].receiver.name+"</span>"+
                    "<p>"+res[i].message+"</p>"+
                    "<span class='chat-date left-align'>"+res[i].date+"</span>"+
                    "</div>"+
                  "</div>";
                "</div>";
              }else{
                var newComment = ""+
                  "<div class='message-bubble'>"+
                    "<div class='chat-bubble round btm-right right'>"+
                      "<div class='chat-text'>"+
                      "<span class='chat-photographer-photo'>P</span>"+
                      "<span class='chat-name right-align'>"+res[i].sender.name+"</span>"+
                      "<p>"+res[i].message+"</p>"+
                      "<span class='chat-date right-align'>"+res[i].date+"</span>"+
                      "</div>"+
                    "</div>"+
                  "</div>";
              }
              actual.append(newComment);
            }
          }
        }
      });
      $('#commentsModal').modal('open');
    }

    function reloadPhotos(){
      $.ajax({
        type: "POST",
        url: "/studio/ensaios/"+{{ $assay->id }}+"/fotos",
        success: function( photos ) {
          $("#ng2").nanogallery2('destroy');
          arrayItens = [];

          photos.forEach(function(photo){
            var href = "{{ asset('storage/images/')}}" +"/"+ photo.path;
            var datangThumb = "{{ asset('storage/images/thumb/')}}" +"/"+ photo.path;
            var photoname = photo.image_name;
            arrayItens.push({
                src : href,
                srct : datangThumb,
                title : photoname
              });
            });
          $("#ng2").nanogallery2({
            galleryDisplayMode: 'moreButton',
            gallerySorting: 'titleAsc',
            galleryTheme: 'light',
            thumbnailSelectable: true, // allow select thumbnailSelectable
            thumbnailLabel: { position: 'overImageOnBottom', hideIcons: true, display: true, titleMaxLength: 30 },
            thumbnailToolbarImage: { topLeft: 'select', topRight : 'share' }, //download, share, cart, info
            viewerToolbar: { standard :'minimizeButton, infoButton, downloadButton, linkOriginalButton, label, fullscreenButton'},
            locationHash: false,
            items:arrayItens
          });
        }
      });
    }

    Dropzone.options.dropzoneUploader = {
      paramName: "file", // The name that will be used to transfer the file
      maxFilesize: 8, // MB,
      acceptedFiles: 'image/*',
      params: { 'id' : $("input[name='ensaio_id']").val() }, // parametro a ser enviado para o servidor
      addRemoveLinks: true,
      accept: function(file, done) {
        if (file.name == "justinbieber.jpg") {
          done("Naha, you don't.");
        }
        else { done(); }
      },
      removedfile: function(file) {
        var name = file.name;
        if (name) {
          $.ajax({
            type: 'POST',
            url: "/removeImageAssay/"+name,  //passes the image name to  the method handling this url to //remove file
            dataType: 'json',
            success: function(resp){
              if(resp.status == true){
                Toast(resp.Message, 2000);
              }
            }
          });
        }
          var _ref;
          return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
      },
      queuecomplete: function(){
        reloadPhotos();
        $("#ng2EmptyMessage").addClass('hide');
        $("#ng2").nanogallery2('reload');
        $("#ng2").removeClass('hide');
        $("#ng2").nanogallery2('refresh');
      }
    };

    $(document).ready(function(){
      window.setTimeout(function() {
        if(window.location.href.indexOf("findComment") != -1){
          var id_comment = window.location.href.split("/").pop(-1);
          openComments(false, false, id_comment);
        }
      }, 1500);
    });

  </script>

@endsection
