@extends('studio.studio')
{{-- <link rel="stylesheet" href="{{ asset('ckeditor/css/samples.css')}}"> --}}
{{-- <link rel="stylesheet" href="{{ asset('ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css') }}"> --}}
@section('content')
  <div class="container">
    {!! Form::open(['method'=>'POST', 'action'=>'ContractController@store']) !!}
    {{-- ---------------------- MODAL ---------------------------- --}}
    <div id="modal_add_contract_type" class="modal bottom-sheet">
      <div class="modal-content">
        <div class="row">
          <div class="col s12">
            <h5 class="black-text">Cadastrando tipo de contrato</h5>
          </div>
        </div>
        <div class="row">
          <div class="col s4"></div>
          <div class="col s4">
            {!! Form::label('description_type', 'Tipo:') !!}
            {!! Form::text('description_type') !!}
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="col s12">
          <div class="col s4"></div>
          <div class="col s4">
            <a id="saveContractType" class="modal-action modal-close waves-effect waves-green btn">Salvar</a>
          </div>
        </div>
      </div>
    </div>
    {{-- -------------------- FIM MODAL -------------------------- --}}
      <div class="section white">
        <div class="container">
        <div class="row" id="pre_register">
          <div class="col s12">
            <h5 class="grey-text">Cadastro de Contratos</h5>
            <div class="row">
              <div class="input-field col s10">
                {!! Form::text('description') !!}
                {!! Form::label('description', 'Nome:') !!}
              </div>
            </div>
            <div class="row">
              <div class="input-field col s8">
                  {!! Form::select('client_id', $clients); !!}
                  {!! Form::label('client_id', 'Cliente:') !!}
              </div>
            </div>
            <div class="row">
              <div class="input-field col s4">
                <i class="small material-icons recuar-top modal-trigger" href="#modal" id="add_contract_type">add_circle</i>
                {!! Form::select('contract_type_id', $typeContracts, ['id'=>'contract_type_id']); !!}
                {!! Form::label('contract_type_id', 'Tipo de Contrato:') !!}
              </div>
              <div class="input-field col s2">
                {!! Form::text('value', null, ['placeholder'=>'R$', 'class'=>'money', 'maxlength'=>'8']) !!}
                {!! Form::label('value', 'Valor:') !!}
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                {!! Form::textarea('text', null, ['id'=>'text']) !!}
              </div>
            </div>
            <div class="row">
              <div class="col s12 center">
                {!! Form::submit('Cadastrar', ['class'=>'btn-large waves-effect waves-light']) !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {!! Form::close() !!}
  </div>

  <script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>


  <script type="text/javascript">
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('[name="_token"]').val()
      }
    });

    function Toast(msg, speed=2000, classe){
      Materialize.toast(msg, speed, classe);
    }

    $("#saveContractType").click(function(){
      description_type = $('#description_type').val();
      _token = $('[name="_token"]').val();
      $.ajax({
        type: "POST",
        url: "/addContractType",
        data: { 'description_type': description_type, '_token': _token },

        success: function( msg ) {
          // console.log(msg);
          toastContent = $('<span>'+msg.success+'</span>');
          Toast(toastContent, 2000);
          updateTypeOfContract();
        },
        error: function(msg){
          toastContent = $('<span>Não foi possível salvar o tipo!</span>');
          Toast(toastContent, 2000);
        },
      });
    })

    function updateTypeOfContract(){
      $.ajax({
        type: "POST",
        url: "/updateContractType",
        success: function( msg ) {
          var opts = msg;
          $('[name="contract_type_id"]').html(" ");
          $.each(opts, function(id, desc) {
            $('[name="contract_type_id"]').append('<option value="' + id + '">' + desc + '</option>');
          });
          $('select').material_select();
        },
        error: function(msg){
          // $("#apagar").html(msg.responseText);
        }
      });
    };

    $("#add_contract_type").click(function(){
      $("#modal_add_contract_type").modal('open');
    })

    $('.modal').modal();
    $('select').material_select();

    CKEDITOR.replace('text');

    var mask = {
      money: function() {
        var el = this
      ,exec = function(v) {
         v = v.replace(/\D/g,"");
         v = new String(Number(v));
         var len = v.length;
         if (1== len)
            v = v.replace(/(\d)/,"0.0$1");
         else if (2 == len)
            v = v.replace(/(\d)/,"0.$1");
         else if (len > 2) {
            v = v.replace(/(\d{2})$/,'.$1');
         }
         return v;
      };

      setTimeout(function(){
         el.value = exec(el.value);
      },1);
   }
}

$(function(){
   $('.money').bind('keypress',mask.money)

   mensagem = '{{ session('error') }}';
   if(mensagem.length > 0){
     toastContent = $('<span>'+mensagem+'</span>');
     Toast(toastContent, 5000);
   }

});


  </script>
@endsection
