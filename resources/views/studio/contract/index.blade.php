@extends('studio.studio')

@section('content')
  @if (count($contracts) == 0)
  {{-- ---------------------- DISCOVERY ---------------------------- --}}
  <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a id="menu" class="btn btn-floating btn-large cyan openDiscovery"><i class="material-icons">lightbulb_outline</i></a>
  </div>
  <div class="tap-target" data-activates="menu">
    <div class="tap-target-content">
      <h5>Ola :)</h5>
      <p>Vimos que você não tem nenhum contrato cadastrado, clique no ícone acima(<i class="material-icons">person_add</i>) para adicionar um novo contrato.</p>
      <p>obs: Você irá precisar de um cliente já cadastrado, para isso clique no menu (<i class="material-icons">menu</i>) no canto superior esquerdo e clique em Clientes.</p>
    </div>
  </div>
  @endif
  {{-- ---------------------- MODAL ---------------------------- --}}
  <div id="modal_delete_contract" class="modal bottom-sheet">
    <div class="modal-content">
      <div class="row">
        <div class="col s12">
          <h4 class="black-text">Você está certo disso?</h4>
        </div>
      </div>
      <div class="row">
        <h5 class="black-text modal-message"></h5>
      </div>
    </div>
    <div class="modal-footer">
      <div class="col s12">
        <div class="col s4"></div>
        <div class="col s4">
          <a id="confirmDelete" class="modal-action modal-close waves-effect waves-green btn confirmDelete">Confirmar</a>
          <a class="modal-action modal-close waves-effect waves-green btn">Voltar</a>
        </div>
        <div class="col s4">
        </div>
      </div>
    </div>
  </div>
    {{-- -------------------- FIM MODAL -------------------------- --}}
  <div class="row">
  <div id="admin" class="col s12">
    <div class="card material-table">
      {!! Form::open(['method' => 'DELETE', 'route' => ['contrato.delete']]) !!}
      <div class="table-header">
        <span class="table-title">Seus contratos cadastrados</span>
        <div class="actions">
          <a href="{{ route('contrato.create') }}" class="modal-trigger waves-effect btn-flat nopadding"><i class="material-icons">person_add</i></a>
          <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
        </div>
      </div>
      <table id="datatable">
        <thead>
          <tr>
            <th>Contrato</th>
            <th>Cliente</th>
            <th>Tipo de Contrato</th>
            <th>Valor</th>
            <th>Ensaio</th>
            <th></th>
            {{-- <th>Ult. Login</th> --}}
          </tr>
        </thead>
        <tbody>
          @foreach($contracts as $contract)
             <tr class="editLink" id="{{ $contract->id }}">
              <td>{{ $contract->description }}</td>
              <td>{{ $contract->client->name }} </td>
              <td>{{ $contract->contract_type->description }} </td>
              <td>{{ $contract->value }} </td>
              <td>-</td>
              <td class="delete delete_contract"><i  class="material-icons red-text delete-tupla">delete_forever</i></td>
            </tr>
            </a>
          @endforeach
        </tbody>
      </table>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<script type="text/javascript">
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('[name="_token"]').val()
    }
  });

  $('.modal').modal();
  $('.tap-target').tapTarget('open');

  $(".openDiscovery").click(function(){
    $('.tap-target').tapTarget('open');
  });

  $(".delete_contract").click(function(e){
    var str = $("#modal_delete_contract .modal-message").html();
    // var client = $(this).closest('tr').find('td').first().text();
    $("#modal_delete_contract .modal-message").html("Você realmente deseja excluir este contrato da sua lista?");
    $(".confirmDelete").attr("id", $(this).closest('tr').attr("id"));
    $("#modal_delete_contract").modal('open');
  });

  $("#confirmDelete").click(function(e){
    var idContract = $(this).attr("id");

    console.log(idContract);

    _token = $('[name="_token"]').val();
    $.ajax({
      type: "POST",
      url: "/studio/contratos/delete",
      data: {
          '_token' : _token,
          '_method' : 'delete',
          'id' : idContract
        },
      success: function( msg ) {
        Toast(msg.success, 3000);
        $('table#datatable').find('tr#'+idContract).remove();
      },
      error: function( msg ) {
        console.log("ERROR:"+msg);
      }
    });
  });

</script>
@endsection
