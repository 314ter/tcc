@extends('studio.studio')

@section('content')
  <div class="container" id="apagar">
    {{-- {!! Form::model($contract, ['method'=>'PATCH', 'action'=>['ContractController@update', $contract->id]]) !!} --}}
    {!! Form::model($contract, ['route'=> ['contrato.update', $contract->id]]) !!}
    <div id="modal_add_contract_type" class="modal bottom-sheet">
      <div class="modal-content">
        <div class="row">
          <div class="col s12">
            <h5 class="black-text">Cadastrando tipo de contrato</h5>
          </div>
        </div>
        <div class="row">
          <div class="col s4"></div>
          <div class="col s4">
            {!! Form::label('description_type', 'Tipo:') !!}
            {!! Form::text('description_type') !!}
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="col s12">
          <div class="col s4"></div>
          <div class="col s4">
            <a id="saveContractType" class="modal-action modal-close waves-effect waves-green btn">Salvar</a>
          </div>
        </div>
      </div>
    </div>
    {{-- -------------------- FIM MODAL -------------------------- --}}
      <div class="section white">
        <div class="container">
        <div class="row" id="pre_register">
          <div class="col s12">
            <h5 class="grey-text">Cadastro de Contratos</h5>
            <div class="row">
              <div class="input-field col s10">
                {!! Form::text('description') !!}
                {!! Form::label('description', 'Nome:') !!}
              </div>
            </div>
            <div class="row">
              <div class="input-field col s8">
                  {!! Form::select('client_id', $clients, $contract->client_id); !!}
                  {!! Form::label('client_id', 'Cliente:') !!}
              </div>
            </div>
            <div class="row">
              <div class="input-field col s4">
                <i class="small material-icons recuar-top modal-trigger" href="#modal" id="add_contract_type">add_circle</i>
                {!! Form::select('contract_type_id', $typeContracts, $contract->contract_type_id,['id'=>'contract_type_id']); !!}
                {!! Form::label('contract_type_id', 'Tipo de Contrato:') !!}
              </div>
              <div class="input-field col s2">
                {!! Form::text('value', null, ['placeholder'=>'R$', 'maxlength'=>'8']) !!}
                {!! Form::label('value', 'Valor:') !!}
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                {!! Form::textarea('text', null, ['id'=>'text']) !!}
              </div>
            </div>
            <div class="row">
              <div class="col s12 center">
                {!! Form::submit('Editar', ['class'=>'btn-large waves-effect waves-light']) !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {!! Form::close() !!}
  </div>

  <script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>


  <script type="text/javascript">
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('[name="_token"]').val()
      }
    });

    $("#saveContractType").click(function(){
      description_type = $('#description_type').val();
      _token = $('[name="_token"]').val();
      $.ajax({
        type: "POST",
        url: "/addContractType",
        data: { 'description_type': description_type, '_token': _token },

        success: function( msg ) {
          toastContent = $('<span>'+msg.success+'</span>');
          Toast(toastContent);
          updateTypeOfContract();
        },
        error: function(msg){
          $("#apagar").html(msg.responseText);
          toastContent = $('<span>Não foi possível salvar o tipo!</span>');
          Toast(toastContent);
        },
      });
    })

    function Toast(msg){
      Materialize.toast(msg, 2000);
    }

    function updateTypeOfContract(){
      $.ajax({
        type: "POST",
        url: "/updateContractType",
        success: function( msg ) {
          var opts = msg;
          $('[name="contract_type_id"]').html(" ");
          $.each(opts, function(id, desc) {
            $('[name="contract_type_id"]').append('<option value="' + id + '">' + desc + '</option>');
          });
          $('select').material_select();
        },
        error: function(msg){
          // $("#apagar").html(msg.responseText);
        }
      });
    };

    $("#add_contract_type").click(function(){
      $("#modal_add_contract_type").modal('open');
    })

    CKEDITOR.replace('text');
  </script>


@endsection
