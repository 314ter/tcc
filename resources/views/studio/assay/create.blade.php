@extends('studio.studio')

@section('content')
  <style>
     #map {
      height: 400px;
      width: 100%;
     }
     #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }
  </style>
  <link rel="stylesheet" href="{{ asset('/css/dropify.min.css') }}">
  <div class="container">
    {!! Form::open(['method'=>'POST', 'action'=>'AssayController@store']) !!}
      <div class="section white">
        {!! Form::hidden('released', 'f') !!}
        <div class="container">
        <div class="row" id="pre_register">
          <div class="col s12">
            <h5 class="grey-text">Cadastro de Ensaios</h5>
            <div class="row"></div>
            <div class="row"></div>
            <div class="row"></div>
            <div class="row">
              <div class="input-field col s8">
                  {!! Form::select('contract_id', $contracts); !!}
                  {!! Form::label('contract_id', 'Contrato:') !!}
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                {!! Form::text('name') !!}
                {!! Form::label('name', 'Nome do Ensaio:') !!}
              </div>
              <div class="col s4"></div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                {!! Form::text('description') !!}
                {!! Form::label('description', 'Descrição:') !!}
              </div>
              <div class="col s4"></div>
            </div>
            <div class="row">
              <div class="input-field col s4">
                {!! Form::label('_datetime', 'Data do Ensaio:') !!}
                {!! Form::text('_datetime', null, ['class'=>'datepicker']) !!}
              </div>
              <div class="input-field col s4">
                {!! Form::label('date_time', 'Hora do Ensaio:') !!}
                {!! Form::text('date_time', null, ['class'=>'timepicker']) !!}
              </div>
              <div class="input-field col s4">
                {!! Form::label('final_date', 'Data Final para seleção:') !!}
                {!! Form::text('final_date', null, ['class'=>'datepicker']) !!}
              </div>
            </div>
                {!! Form::hidden('local', null, ['id'=>'localizacao']) !!}
              <div class="input-field col s5 m5 l5">
                <input id="pac-input" class="controls" type="text" placeholder="Pesquise o Local">
              </div>
              <div class="col s12 m12 l12" id="map">
              </div>
            </div>

            <div class="row">
              <div class="col s12 m12 l12">
                <div id="fileList">
                </div>
              </div>
            </div>

            {{-- <div class="row">
              <div class="col s12">
                {!! Form::file('image_name', ['multiple']) !!}
              </div>
            </div> --}}
            <div class="col s4"></div>
            <div class="row">
              <div class="col s12 center">
                {!! Form::submit('Cadastrar', ['class'=>'btn-large waves-effect waves-light']) !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {!! Form::close() !!}
  </div>

  <script type="text/javascript">

  if(mensagem.length > 0){
    toastContent = $('<span>'+mensagem+'</span>');
    Toast(toastContent, 5000);
  }
      function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -31.7641417, lng: -52.34167814},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        marker = new google.maps.Marker({
          map: map,
          draggable: false,
          position: {lat: 0, lng: 0}
        });

        google.maps.event.addListener(map, 'click', function (event) {
          $("#localizacao").val(event.latLng.lat()+"|"+event.latLng.lng());
          marker.setPosition(event.latLng);
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };


            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location,
              draggable:false,
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }

      function toggleBounce() {
        if (marker.getAnimation() !== null) {
          marker.setAnimation(null);
        } else {
          marker.setAnimation(google.maps.Animation.BOUNCE);
        }
      }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCToVLshwVKJygrIldDGGOF8_qvom1sMic&libraries=places&callback=initAutocomplete"
         async defer></script>

@endsection
