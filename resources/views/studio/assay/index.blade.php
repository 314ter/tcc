@extends('studio.studio')

@section('content')
  @if (count($assays) == 0)
  {{-- ---------------------- DISCOVERY ---------------------------- --}}
  <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a id="menu" class="btn btn-floating btn-large cyan openDiscovery"><i class="material-icons">lightbulb_outline</i></a>
  </div>
  <div class="tap-target" data-activates="menu">
    <div class="tap-target-content">
      <h5>Ola :)</h5>
      <p>Vimos que você não tem nenhum ensaio cadastrado, clique no ícone acima(<i class="material-icons">person_add</i>) para adicionar um novo ensaio.</p>
      <p>obs: Você irá precisar de um contrato já cadastrado, para isso clique no menu (<i class="material-icons">menu</i>) no canto superior esquerdo e clique em Contratos.</p>
    </div>
  </div>
  @endif
  {{-- ---------------------- MODAL ---------------------------- --}}
  <div id="modal_delete_assay" class="modal bottom-sheet">
    <div class="modal-content">
      <div class="row">
        <div class="col s12">
          <h4 class="black-text">Você está certo disso?</h4>
        </div>
      </div>
      <div class="row">
        <h5 class="black-text modal-message"></h5>
      </div>
    </div>
    <div class="modal-footer">
      <div class="col s12">
        <div class="col s4"></div>
        <div class="col s4">
          <a id="confirmDelete" class="modal-action modal-close waves-effect waves-green btn confirmDelete">Confirmar</a>
          <a class="modal-action modal-close waves-effect waves-green btn">Voltar</a>
        </div>
        <div class="col s4">
        </div>
      </div>
    </div>
  </div>
    {{-- -------------------- FIM MODAL -------------------------- --}}
  <div class="row">
  <div id="admin" class="col s12">
    <div class="card material-table">
      {!! Form::open(['method' => 'DELETE', 'route' => ['ensaio.delete']]) !!}
      <div class="table-header">
        <span class="table-title">Seus ensaios cadastrados</span>
        <div class="actions">
          <a href="{{ route('ensaio.create') }}" class="modal-trigger waves-effect btn-flat nopadding"><i class="material-icons">person_add</i></a>
          <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
        </div>
      </div>
      <table id="datatable">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Data</th>
            <th>Status</th>
            <th>Liberado</th>
            <th>Galeria</th>
            <th></th>
            {{-- <th>N° de Fotos</th> --}}
            {{-- <th>Escolhida</th> --}}
          </tr>
        </thead>
        <tbody>
          @foreach($assays as $assay)
              <tr class="editLink" id="{{ $assay->id }}">
                <td>{{ $assay->name }}</td>
                <td>{{ $assay->datetime }}</td>
                <td>{{ $assay->status }}</td>
                <td>{!! $assay->released==1 ? "Sim <i class='material-icons green-text'>check_circle</i>" : "Não <i class='material-icons red-text'>cancel</i>" !!}</td>
                {{-- <td class="library"><a href="{{ url('/studio/ensaios/'.$assay->id.'/fotos') }}"><i class="material-icons">photo_library</i></a></td> --}}
                <td><a class="waves-effect waves-light btn" href="{{ url('/studio/ensaios/'.$assay->id.'/fotos') }}"><i class="material-icons black-text ">photo_library</i> {!! count($assay->photos) !!} Fotos</a></td>
                <td class="delete delete_assay"><i  class="material-icons red-text delete-tupla">delete_forever</i></td>
              </tr>
          @endforeach
        </tbody>
      </table>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<script type="text/javascript">
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('[name="_token"]').val()
    }
  });

  $('.modal').modal();
  $('.tap-target').tapTarget('open');

  $(".openDiscovery").click(function(){
    $('.tap-target').tapTarget('open');
  });

  $(".delete_assay").click(function(e){
    var str = $("#modal_delete_assay .modal-message").html();
    // var client = $(this).closest('tr').find('td').first().text();
    $("#modal_delete_assay .modal-message").html("Você realmente deseja excluir este ensaio da sua lista?");
    $(".confirmDelete").attr("id", $(this).closest('tr').attr("id"));
    $("#modal_delete_assay").modal('open');
  });

  $("#confirmDelete").click(function(e){
    var idAssay = $(this).attr("id");
    _token = $('[name="_token"]').val();
    $.ajax({
      type: "POST",
      url: "/studio/ensaios/delete",
      data: {
          '_token' : _token,
          '_method' : 'delete',
          'id' : idAssay
        },
      success: function( msg ) {
        Toast(msg.success, 3000);
        $('table#datatable').find('tr#'+idAssay).remove();
      },
      error: function( msg ) {
        console.log("ERROR:"+msg.error);
      }
    });
  });
</script>
@endsection
