@extends('studio.studio')

@section('content')

  <div class="container">
    {!! Form::open(['method'=>'POST', 'action'=>'ClientController@store']) !!}
    {{-- {{ csrf_field() }} --}}
    <?php // TODO: ENVIAR ID DO FOTOGRAFO JUNTO ?>
      <div class="section white">
        <div class="container">
        <div class="row" id="pre_register">
          <div class="col s12">
            <h5 class="grey-text">Cadastro de Clientes</h5>
              {{-- @if(count($errors))
								<div class="row">
									@foreach($errors->all() as $error)
									<p class="status error">{{ $error }}</p>
									@endforeach
								</div>
							@endif --}}
              <div class="row">
                <div class="input-field col s12">
                  {!! Form::text('name') !!}
                  {!! Form::label('name', 'Nome Completo:') !!}
                </div>
                <div class="col s4"></div>
              </div>
              <div class="row">
                <div class="col s6">
                  <p class="checkbox-input">
                    {!! Form::checkbox('juridical', 'yes', false, ['id'=>'juridical']) !!}
                    {!! Form::label('juridical', 'Pessoa Jurídica?', ['id'=>'juridicalLabel']) !!}
                  </p>
                </div>
                <div id="divCPF" class="input-field col s6">
                  {!! Form::text('cpf', null, ['id'=>'cpf']) !!}
                  {!! Form::label('cpf', 'CPF:') !!}
                </div>
                <div id="divCNPJ" class="input-field col s6 hide">
                  {!! Form::text('cnpj', null, ['id'=>'cnpj']) !!}
                  {!! Form::label('cnpj', 'CNPJ:') !!}
                </div>
              </div>
              <div class="row">
                <div class="input-field col s4">
                  {!! Form::text('postal_code', null, ['id'=>'postal_code']) !!}
                  {!! Form::label('postal_code', 'CEP:') !!}
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  {!! Form::text('address_name', null) !!}
                  {!! Form::label('address_name', 'Endereço:') !!}
                </div>
              </div>
              <div class="row">
                <div class="input-field col s6">
                  {!! Form::text('district', null) !!}
                  {!! Form::label('district', 'Bairro:') !!}
                </div>
                <div class="input-field col s6">
                  {!! Form::text('city', null) !!}
                  {!! Form::label('city', 'Cidade:') !!}
                </div>
              </div>
              <div class="row">
                <div class="input-field col s6">
                  {!! Form::select('state', [
                    'AC'=>'Acre',
                    'AL'=>'Alagoas',
                    'AP'=>'Amapá',
                    'AM'=>'Amazonas',
                    'BA'=>'Bahia',
                    'CE'=>'Ceará',
                    'DF'=>'Distrito Federal',
                    'ES'=>'Espírito Santo',
                    'GO'=>'Goiás',
                    'MA'=>'Maranhão',
                    'MT'=>'Mato Grosso',
                    'MS'=>'Mato Grosso do Sul',
                    'MG'=>'Minas Gerais',
                    'PA'=>'Pará',
                    'PB'=>'Paraíba',
                    'PR'=>'Paraná',
                    'PE'=>'Pernambuco',
                    'PE'=>'Piauí',
                    'RJ'=>'Rio de Janeiro',
                    'RN'=>'Rio Grande do Norte',
                    'RS'=>'Rio Grande do Sul',
                    'RO'=>'Rondônia',
                    'RR'=>'Roraima',
                    'SC'=>'Santa Catarina',
                    'SP'=>'São Paulo',
                    'SE'=>'Sergipe',
                    'TO'=>'Tocantins'
                  ]); !!}
                  {!! Form::label('district', 'Estado:') !!}
                </div>
                <div class="input-field col s6">
                  {!! Form::text('country', null, ['maxlength'=>2]) !!}
                  {!! Form::label('country', 'País:') !!}
                </div>
                <?php // TODO: LOCALIZAÇãO ?>
              </div>
              <div class="row">
                <div class="input-field col s4">
                  {!! Form::select('marital_status', [
                    'SOLTEIRO'=>'Solteiro',
                    'CASADO'=>'Casado',
                    'SEPARADO'=>'Separado',
                    'DIVORCIADO'=>'Divorciado',
                    'VIÚVO'=>'Viúvo'
                  ]); !!}
                  {!! Form::label('marital_status', 'Estado Civil:') !!}
                </div>
                <div class="input-field col s4">
                  {!! Form::select('gender', [
                    'F'=>'Feminino',
                    'M'=>'Masculino'
                  ]); !!}
                  {!! Form::label('gender', 'Sexo:') !!}
                </div>
                <div class="input-field col s4">
                  {!! Form::label('birthdate', 'Data de Nascimento:') !!}
                  {!! Form::text('birthdate', null, ['class'=>'datepicker']) !!}
                </div>
              </div>
              <div class="row">
                <div class="input-field col s4">
                  {!! Form::tel('phone', null, ['placeholder'=>'(99)99999-9999', 'id'=>'tel']) !!}
                  {!! Form::label('phone', 'Telefone:') !!}
                </div>
                <div class="input-field col s8">
                  {!! Form::text('email',null) !!}
                  {!! Form::label('email', 'E-mail:') !!}
                </div>
              </div>
              <div class="row">

                <div class="input-field col s4">
                </div>
              </div>
              <div class="row">
                <div class="col s12 center">
                  {!! Form::submit('Cadastrar', ['class'=>'btn-large waves-effect waves-light']) !!}
                  {{-- <a id="btn-register-user" class="btn-large waves-effect waves-light">Cadastrar <i class="right  material-icons">done</i></a> --}}
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
    {!! Form::close() !!}
  </div>
  <script src="{{asset('js/vanilla-masker.min.js')}}"></script>
  <script type="text/javascript">
    function Toast(msg, speed=2000, classe){
      Materialize.toast(msg, speed, classe);
    }

    var erros = {!! json_encode($errors->all()) !!};
    if(erros != 'undefined' && erros.length > 0){
      erros.forEach(function(value, index){
        Toast(value, 8000, "red-text");
      });
    }

    $("#juridicalLabel").click(function(){
      if($("#juridical").prop("checked") == false){
        $("#divCPF").toggleClass('hide');
        $("#divCNPJ").toggleClass('hide');
      }else{
        $("#divCPF").toggleClass('hide');
        $("#divCNPJ").toggleClass('hide')
      }
    })
    function inputHandler(masks, max, event) {
    	var c = event.target;
    	var v = c.value.replace(/\D/g, '');
    	var m = c.value.length > max ? 1 : 0;
    	VMasker(c).unMask();
    	VMasker(c).maskPattern(masks[m]);
    	c.value = VMasker.toPattern(v, masks[m]);
    }

    var telMask = ['(99) 9999-99999', '(99) 99999-9999'];
    var tel = document.querySelector('#tel');
    VMasker(tel).maskPattern(telMask[0]);
    tel.addEventListener('input', inputHandler.bind(undefined, telMask, 14), false);

    var cpfMask = ['999.999.999-99'];
    var doc = document.querySelector('#cpf');
    VMasker(doc).maskPattern(cpfMask[0]);
    doc.addEventListener('input', inputHandler.bind(undefined, cpfMask, 15), false);

    var cnpjMask = ['99.999.999/9999-99'];
    var cnpj = document.querySelector('#cnpj');
    VMasker(cnpj).maskPattern(cnpjMask[0]);
    cnpj.addEventListener('input', inputHandler.bind(undefined, cnpjMask, 19), false);

    var postalMask = ['99999-999'];
    var postal = document.querySelector('#postal_code');
    VMasker(postal).maskPattern(postalMask[0]);
    postal.addEventListener('input', inputHandler.bind(undefined, postalMask, 10), false);
  </script>
@endsection
