@extends('studio.studio')

@section('content')

  @if (count($clients) == 0)
  {{-- ---------------------- DISCOVERY ---------------------------- --}}
  <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a id="menu" class="btn btn-floating btn-large cyan openDiscovery"><i class="material-icons">lightbulb_outline</i></a>
  </div>
  <div class="tap-target" data-activates="menu">
    <div class="tap-target-content">
      <h5>Ola :)</h5>
      <p>Vimos que você não tem nenhum cliente cadastrado, clique no ícone acima(<i class="material-icons">person_add</i>) para adicionar um novo cliente.</p>
    </div>
  </div>
  @endif
  {{-- ---------------------- MODAL ---------------------------- --}}
  <div id="modal_delete_client" class="modal bottom-sheet">
    <div class="modal-content">
      <div class="row">
        <div class="col s12">
          <h4 class="black-text">Você está certo disso?</h4>
        </div>
      </div>
      <div class="row">
        <h5 class="black-text modal-message"></h5>
      </div>
    </div>
    <div class="modal-footer">
      <div class="col s12">
        <div class="col s4"></div>
        <div class="col s4">
          <a id="confirmDelete" class="modal-action modal-close waves-effect waves-green btn confirmDelete">Confirmar</a>
          <a class="modal-action modal-close waves-effect waves-green btn">Voltar</a>
        </div>
        <div class="col s4">
        </div>
      </div>
    </div>
  </div>
  {{-- -------------------- FIM MODAL -------------------------- --}}
  <div class="row">
  <div id="admin" class="col s12">
    <div class="card material-table">
      {!! Form::open(['method' => 'DELETE', 'route' => ['cliente.delete']]) !!}
      <div class="table-header">
        <span class="table-title">Seus clientes cadastrados</span>
        <div class="actions">
          <a href="{{ route('cliente.create') }}" class="modal-trigger waves-effect btn-flat nopadding"><i class="material-icons">person_add</i></a>
          <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
        </div>
      </div>
      <table id="datatable">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Data de Nascimento</th>
            <th>Telefone</th>
            <th>Email</th>
            <th></th>
            {{-- <th>Ult. Login</th> --}}
          </tr>
        </thead>
        <tbody>
          @foreach($clients as $client)
              <tr class="editLink" id="{{ $client->id }}">
                <td>{{ $client->name }}</td>
                <td>{{ $client->birthdate }}</td>
                <td>{{ $client->phone }}</td>
                <td>{{ $client->email }}</td>
                <td class="delete delete_client"><i  class="material-icons red-text delete-tupla">delete_forever</i></td>
              </tr>
          @endforeach
        </tbody>
      </table>
      {!! Form::close() !!}
    </div>
  </div>
</div>
<script type="text/javascript">
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('[name="_token"]').val()
    }
  });

  $('.modal').modal();
  $('.tap-target').tapTarget('open');

  $(".openDiscovery").click(function(){
    $('.tap-target').tapTarget('open');
  });

  $(".delete_client").click(function(e){
    var str = $("#modal_delete_client .modal-message").html();
    var client = $(this).closest('tr').find('td').first().text();
    $("#modal_delete_client .modal-message").html("Você realmente deseja excluir "+client+" da sua lista de clientes?");
    $(".confirmDelete").attr("id", $(this).closest('tr').attr("id"));
    $("#modal_delete_client").modal('open');
  });

  $("#confirmDelete").click(function(e){
    var idClient = $(this).attr("id");

    _token = $('[name="_token"]').val();
    $.ajax({
      type: "POST",
      url: "/studio/contracts/delete",
      data: {
          '_token' : _token,
          '_method' : 'delete',
          'id' : idClient
        },
      success: function( msg ) {
        Toast(msg.success, 3000);
        $('table#datatable').find('tr#'+idClient).remove();
      },
      error: function( msg ) {
        console.log("ERROR:"+msg.error);
      }
    });
  });


</script>
@endsection
