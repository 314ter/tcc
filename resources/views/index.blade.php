@extends('base')
@section('navbar')
  <header>
    <nav class="top-nav" role="navigation">
      <div class="nav-wrapper container">
        <a id="logo-container" href="#" class="brand-logo">
          <img class="top-logo" src="images/logobranco.png">
        </a>
        <ul class="right hide-on-med-and-down">
          <li><a href="#">HOME</a></li>
          <li><a href="#contato">CONTATO</a></li>
          <li><a href="/login">LOGIN</a></li>
          {{-- <li><a href="#">SOBRE</a></li>
          <li><a href="#">CONTATO</a></li> --}}
        </ul>
        <ul id="nav-mobile" class="side-nav">
          <li><a href="#">HOME</a></li>
          <li><a href="#contato">CONTATO</a></li>
          <li><a href="#">LOGIN</a></li>
          {{-- <li><a href="#">SERVIÇOS</a></li>
          <li><a href="#">EXPOSIÇÃO</a></li>
          <li><a href="#">SOBRE</a></li>
          <li><a href="#">CONTATO</a></li> --}}
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
      </div>
    </nav>

  </header>
@endsection

@section('body')

  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container home">

        <h1 class="header center black-text">[ PhotoBooker ]</h1>
        <div class="row center">
          <h4 class="header col s12 grey-text text-darken-4">Você registra os momentos, e nós fazemos todo o resto.</h4>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="images/bgPiter.jpg" alt="Unsplashed background img 1"></div>
  </div>

  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s12">
          <h4 class="grey-text text-lighten-2 center">Comece a utilizar o Photobooker em 3 passos</h4>
          <div class="row">
            <div class="col s12 m4">
              <div class="icon-block">
                <h1 class="center grey-text text-darken-4">1</h1>
                <h5 class="center grey-text text-darken-1"><strong>Inicie fazendo o cadastro no sistema</strong></h5>
                <p class="grey-text text-lighten-2 center">Precisamos de algumas informações para identificá-lo no sistema do Photobooker.</p>
              </div>
            </div>
            <div class="col s12 m4">
              <div class="icon-block">
                <h1 class="center grey-text text-darken-4">2</h1>
                <h5 class="center grey-text text-darken-1"><strong>Confirme seu cadastro</strong></h5>
                <p class="grey-text text-lighten-2 center">Enviaremos um e-mail confimar e validar seu registro.</p>
              </div>
            </div>
            <div class="col s12 m4">
              <div class="icon-block">
                <h1 class="center grey-text text-darken-4">3</h1>
                <h5 class="center grey-text text-darken-1"><strong>Comece a gerenciar seus trabalhos</strong></h5>
                <p class="grey-text text-lighten-2 center">Com as ferramentas disponibilizadas pelo Photobooker, será muito mais fácil de gerenciar.</p>
              </div>
            </div>
          </div>
          <div class="row center">
            <a id="btn-init" class="btn-large waves-effect waves-light #e1f5fe">Iniciar <i class="right  material-icons">camera_enhance</i></a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
          <div class="container">
            <h3 class="header col s12 grey-text text-darken-4 ">Entre para a família [:o:]Photobooker e não gaste mais tempo com agendas, planilhas, e reuniões para escolha de fotos, deixe que fazemos isso para você.</h3>
          </div>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="images/bgPicjumbo.jpg" alt="Unsplashed background img 2"></div>
  </div>
  {!! Form::open(['method'=>'POST', 'action'=>'UserController@storePreRegister','id'=>'formNewUser']) !!}
    <div class="container">
      <div class="section">
        <div class="row" id="pre_register">
          <div class="col s12">
            <div class="col s12">
              <h5 class="grey-text text-lighten-2">Deseja fazer parte da família Photobooker?</h5>
              <div class="space"></div>
              <div class="col s12">
                <div class="row">
                  <div class="input-field col s10">
                    {!! Form::text('name', null, ['placeholder'=>'nome completo']) !!}
                    {!! Form::label('name', 'Nome Completo:') !!}
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s5">
                    {!! Form::email('email', null, ['placeholder'=>'e-mail', 'id'=>'usermail']) !!}
                    {!! Form::label('email', 'E-mail:') !!}
                  </div>
                  <div class="input-field col s5">
                    {!! Form::password('password', null, ['placeholder'=>'senha', 'id'=>'userpass']) !!}
                    {!! Form::label('password', 'Senha:') !!}
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s10">
                    <div class="input-field col s4">
                      <a id="btn-register" class="btn-large waves-effect waves-light">Cadastrar <i class="right  material-icons">done</i></a>
                    </div>
                    <div class="input-field col s4">
                      <br>
                      <a href="{{ route('resendMail') }}" class="right waves-effect">Não recebeu e-mail? <i class="right  material-icons">mail_outline</i></a>
                    </div>
                    <div class="input-field col s4">
                      <br>
                      <a href="{{ route('login') }}" class="right waves-effect">Já possui cadastro? <i class="right  material-icons">perm_identity</i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  {!! Form::close() !!}

  <div class="modal register">
    <div class="modal-content">
      <h4 id="sending-mail">Enviando E-mail</h4>
      <div class="center">
        <div class="space"></div>
        <div id="responseEmail"></div>
        <!-- loader Structure -->
        <div class="loader">
          <div class="spinner-layer spinner-blue-only">
            <div class="circle-clipper left">
              <div class="circle"></div>
            </div><div class="gap-patch">
              <div class="circle"></div>
            </div><div class="circle-clipper right">
              <div class="circle"></div>
            </div>
          </div>
        </div>
        <!-- loader Structure Ends-->
  <!-- $(".waiting-mail").removeClass("active") -->
        <div class="space"></div>
      </div>
    </div>
  </div>

@endsection

@section('footer');

  <footer class="page-footer" id="pagefooter">
    <div class="container">
      <div class="row">
        <div class="col l4 s12">
          <div class="row"></div>
          <div class="row"></div>
          <div class="row"></div>
          <img src="images\LOGO.png" alt="logo photobooker">
          <div class="row"></div>
          <div class="row"></div>
          <div class="row"></div>
          <img src="images\logobranco.png" alt="logo photobooker">
        </div>
         <div class="col l3 s12">
        {{--  <h5 class="white-text center-align">Redes-sociais</h5>
          <ul class='center-align'>
            <div class="row"></div>
            <div class="row"></div>
            <div class="row"></div>
            <li><a class="white-text" href="#!"><img src="images\facebook.svg" class="small-ico" alt=""></a></li>
            <div class="row"></div>
            <div class="row"></div>
            <li><a class="white-text" href="#!"><img src="images\instagram.svg" class="small-ico" alt=""></a></li>
            <div class="row"></div>
            <div class="row"></div>
            <li><a class="white-text" href="#!"><img src="images\pinterest.svg" class="small-ico" alt=""></a></li>
          </ul>--}}
        </div>
        <div id="contato" class="col l5 s12 center-align">
          <h5 class="white-text">Contato</h5>
          <div class="container">
            <div class="row left-align">
              <div class="col s12 m12 l12">
                <ul>
                  <li><label for="">Nome Completo:</label><input type="text" name="nomecliente"></li>
                  <li><label for="">Email:</label><input type="text" name="emailcliente"</li>
                  <li><label for="">Mensagem:</label><textarea name="mensagemcliente" class="materialize-textarea"></textarea></li>
                  <li class='right-align'>
                    <button class="btn waves-effect waves-light" id="sendContact" type="submit" name="action">Enviar
                      <i class="material-icons right">send</i>
                    </button>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-copyright ">
      <div class="container center-align">
        Desenvolvido por <a class="brown-text text-lighten-3" href="">Piter Ortiz</a> | 2017
      </div>
    </div>
  </footer>
  <script type="text/javascript">

  function Toast(msg, speed=2000, classe){
    Materialize.toast(msg, speed, classe);
  }

  $("#sendContact").click(function(){
    window.setTimeout(function() {
        Toast("Mensagem enviada com sucesso, logo entraremos em contato!");
    }, 1500);
  })


  $(document).ready(function(){
      msg = [
        @foreach ($errors->all() as $error)
          "{{ $error }}",
        @endforeach
      ];
      msg.forEach(function(error){
        // console.log(error);
        toastContent = $('<span>'+error+'</span>');
        Toast(toastContent, 5000);
      });
  })
  </script>
@endsection
