@extends('full')
@section('contentFull')

  {!! Form::open(['method'=>'POST', 'action'=>'UserController@resendMail']) !!}
  <div class="vertical-center">
    <div class="white">
      <div class="row">
        <div class="col s12 center">
          <img height="150rem" src="{{ asset('images/LOGO.png') }}" alt="">
        </div>
      </div>
      @if(!isset($pre))
        @if(count($errors)>0)
          <div class="row">
            <div class="col s0 m3 l3"></div>
            <div class="col s12 m6 l6">
              <div class="card orange lighten-1">
                <div class="card-content black-text">
                  <span class="card-title">Ops :(</span>
                  @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        @endif
        <div class="container">
          <div class="row">
            <div class="col s12">
              <h5>Olá amigo, percebi que o e-mail não foi enviado, correto?</h5>
              <h6>Por favor, digite o e-mail cadastrado no campo abaixo para enviarmos novamente o e-mail!</h6>
            </div>
          </div>
          <div class="row">
            <div class="col s2 m2 l2">
            </div>
            <div class="col s8 m8 l8">
              {!! Form::email('email', null, ['placeholder'=>'e-mail', 'id'=>'usermail']) !!}
            </div>
          </div>
          <div class="row">
            <div class="col s12 center">
              {!! Form::submit('Re-enviar', ['class'=>'btn waves-effect waves-light']) !!}
            </div>
          </div>
          <div class="row">
            <div class="col s12 center">
              <h6 class="recuo"><i>OBS: Verifique sua caixa de SPAM, talvez o e-mail tenha parado lá.</i></h6>
            </div>
          </div>
        </div>
      @else
        <div class="container">
          <div class="row">
            <div class="col s12">
              <h4><i class="material-icons">mail_outline</i> E-mail enviado :)</h4>
            </div>
          </div>
          <div class="row">
            <h6>Aguarde um instante que irá chegar um e-mail de confirmação no seu e-mail.</h6>
            <h6 class="recuo"><i>OBS: Verifique sua caixa de SPAM, talvez o e-mail tenha parado lá.</i></h6>
          </div>
      @endif
    </div>
  </div>

    {!! Form::close() !!}

@endsection
