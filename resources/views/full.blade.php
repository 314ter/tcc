<!DOCTYPE html>
<html lang="pt">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>[:o:] [Photobooker]</title>
  <link rel="icon" href="images/favicon.png" type="image/x-icon"/>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  {!! MaterializeCSS::include_full() !!}
  <link type="text/css" rel="stylesheet" href="{{ asset('css/full.css') }}">
</head>
<body>
  {{-- <div class="vertical-center"> --}}
  <div class="container">
    @yield('contentFull')
  </div>
  <script src="{{asset('js/vanilla-masker.min.js')}}"></script>
  <script src="{{asset('js/full.js')}}"></script>

</body>
</html>
