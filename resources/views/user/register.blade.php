@extends('full')

@section('contentFull')

  {!! Form::open(['method'=>'POST', 'action'=>'UserController@store', 'id'=>'formUserRegister']) !!}
  {!! Form::hidden('email', $user['email']) !!}

    <div class="white col s12">
      <div class="container">
        <div class="row">
          <div class="col s12 center">
            <img height="180rem" src="{{ asset('images/LOGO.png') }}" alt="">
          </div>
        </div>
        @if(count($errors)>0)
          <div class="row">
            <div class="col s0 m3 l3"></div>
            <div class="col s12 m6 l6">
              <div class="card orange lighten-1">
                <div class="card-content black-text">
                  <span class="card-title">Ops :(</span>
                  @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        @endif
        <div class="row">
          <div class="col s12">
            <h5 class="center-align invalid">
              Seu registro está quase completo.
            </h5>
          </div>
        </div>
        <div class="row center">
          <div class="col s12">
            {{-- <h6>Olá <strong>{{ $user['name'] }}</strong>, lhe foi enviado um e-mail contendo um código, copie o código e cole aqui abaixo :)</h6> --}}
          </div>
        </div>
        <div class="row">
            {{-- {!! Form::text('register_token', old('register_token'), ['placeholder'=>'cole o código aqui', 'class'=>'register_token_field']) !!} --}}
          <div class="input-field col s12">
            {!! Form::text('name', $user->name) !!}
            {!! Form::label('name', 'Nome Completo:') !!}
          </div>
          <div class="col s4"></div>
        </div>
        <div class="row">
          <div class="col s6">
            <p class="checkbox-input">
              {!! Form::checkbox('juridical', 'yes', false, ['id'=>'juridical']) !!}
              {!! Form::label('juridical', 'Pessoa Jurídica?', ['id'=>'juridicalLabel']) !!}
            </p>
          </div>
          <div id="divCPF" class="input-field col s6">
            {!! Form::text('cpf', null, ['id'=>'cpf']) !!}
            {!! Form::label('cpf', 'CPF:') !!}
          </div>
          <div id="divCNPJ" class="input-field col s6 hide">
            {!! Form::text('cnpj', null, ['id'=>'cnpj']) !!}
            {!! Form::label('cnpj', 'CNPJ:') !!}
          </div>
        </div>
        <div class="row">
          <div class="input-field col s4">
            {!! Form::text('postal_code', null, ['id'=>'postal_code']) !!}
            {!! Form::label('postal_code', 'CEP:') !!}
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            {!! Form::text('address_name', null) !!}
            {!! Form::label('address_name', 'Endereço:') !!}
          </div>
        </div>
        <div class="row">
          <div class="input-field col s6">
            {!! Form::text('district', null) !!}
            {!! Form::label('district', 'Bairro:') !!}
          </div>
          <div class="input-field col s6">
            {!! Form::text('city', null) !!}
            {!! Form::label('city', 'Cidade:') !!}
          </div>
        </div>
        <div class="row">
          <div class="input-field col s6">
            {!! Form::select('state', [
              'AC'=>'Acre',
              'AL'=>'Alagoas',
              'AP'=>'Amapá',
              'AM'=>'Amazonas',
              'BA'=>'Bahia',
              'CE'=>'Ceará',
              'DF'=>'Distrito Federal',
              'ES'=>'Espírito Santo',
              'GO'=>'Goiás',
              'MA'=>'Maranhão',
              'MT'=>'Mato Grosso',
              'MS'=>'Mato Grosso do Sul',
              'MG'=>'Minas Gerais',
              'PA'=>'Pará',
              'PB'=>'Paraíba',
              'PR'=>'Paraná',
              'PE'=>'Pernambuco',
              'PE'=>'Piauí',
              'RJ'=>'Rio de Janeiro',
              'RN'=>'Rio Grande do Norte',
              'RS'=>'Rio Grande do Sul',
              'RO'=>'Rondônia',
              'RR'=>'Roraima',
              'SC'=>'Santa Catarina',
              'SP'=>'São Paulo',
              'SE'=>'Sergipe',
              'TO'=>'Tocantins'
            ]); !!}
            {!! Form::label('district', 'Estado:') !!}
          </div>
          <div class="input-field col s6">
            {!! Form::text('country', null, ['maxlength'=>2]) !!}
            {!! Form::label('country', 'País:') !!}
          </div>
          <?php // TODO: LOCALIZAÇãO ?>
        </div>
        <div class="row">
          <div class="input-field col s4">
            {!! Form::label('birthdate', 'Data de Nascimento:') !!}
            {!! Form::text('birthdate', null, ['class'=>'datepicker','data-date-format'=>'mm-dd-yyyy']) !!}
          </div>
          <div class="input-field col s4">
            {!! Form::tel('phone', null, ['id'=>'tel','placeholder'=>'(99)99999-9999']) !!}
            {!! Form::label('phone', 'Telefone:') !!}
          </div>
        </div>
        <div class="row">
          <div class="input-field col s6">
            {!! Form::text('email', $user['email'], ['disabled']) !!}
            {!! Form::label('email', 'E-mail:') !!}
          </div>
          <div class="input-field col s4">
            {{-- {!! Form::password('password', null) !!}
            {!! Form::label('password', 'Senha:') !!} --}}
          </div>
        </div>
        <div class="row">
          <div class="col s12 center">
            <a id="btn-register-user" class="btn-large waves-effect waves-light">Cadastrar <i class="right  material-icons">done</i></a>
          </div>
        </div>
      </div>
    </div>
  {!! Form::close() !!}
<script>
$("#btn-register-user").click(function(){
  $("#formUserRegister").submit();
})
</script>
@endsection
