@extends('full')

@section('contentFull')
  {!! Form::open(['route'=>'cliente.confirmacao']) !!}
  {!! Form::hidden('email', $client['email']) !!}
    <div class="white col s12">
      <div class="container">
        <div class="row">
          <div class="col s12 center">
            <img height="180rem" src="{{ asset('images/LOGO.png') }}" alt="">
          </div>
        </div>
        @if(count($errors)>0)
          <div class="row">
            <div class="col s0 m3 l3"></div>
            <div class="col s12 m12 l12">
              <div class="card orange lighten-1">
                <div class="card-content black-text">
                  <span class="card-title">Ops :(</span>
                    <p>{{ $errors['message'] }}</p>
                </div>
              </div>
            </div>
          </div>
        @else
        <div class="row">
          <div class="col s12">
            <h5 class="center-align invalid">
              Seu registro está quase completo.
            </h5>
          </div>
        </div>
        <div class="row center">
          <div class="col s12">
            <h6>Olá <strong>{{ $client['name'] }}</strong>, conseguimos confirmar seu cadastro, agora, por favor, crie uma senha para acessar o sistema :)</h6>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s8">
            {!! Form::label('password', 'Senha:') !!}
            {!! Form::password('password') !!}
          </div>
          <div class="col s4">
            {!! Form::submit('Enviar', ['class'=>'btn baixar-input waves-effect waves-light']) !!}
          </div>
        </div>
      </div>
    </div>
  {!! Form::close() !!}
@endif
@endsection
