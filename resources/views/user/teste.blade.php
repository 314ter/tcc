@extends('full')

@section('contentFull')

  {!! Form::open(['method'=>'POST', 'action'=>'UserController@store', 'id'=>'formUserRegister']) !!}
  {!! Form::hidden('email', $user['email']) !!}
    <div class="white col s12">
      <div class="row">
        <div class="col s12 center">
          <img height="20%" src="{{ asset('images/LOGO.png') }}" alt="">
        </div>
      </div>
      @if(count($errors)>0)
        <div class="row">
          <div class="col s0 m3 l3"></div>
          <div class="col s12 m6 l6">
            <div class="card orange lighten-1">
              <div class="card-content black-text">
                <span class="card-title">Ops :(</span>
                @foreach ($errors->all() as $error)
                  <p>{{ $error }}</p>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      @endif
      <div class="row">
        <div class="col s12">
          <h5 class="center-align invalid">
            Seu registro está quase completo.
          </h5>
        </div>
      </div>
      <div class="row center">
        <div class="col s12">
          {{-- <h6>Olá <strong>{{ $user['name'] }}</strong>, lhe foi enviado um e-mail contendo um código, copie o código e cole aqui abaixo :)</h6> --}}
        </div>
      </div>
      <div class="row">
          {{-- {!! Form::text('register_token', old('register_token'), ['placeholder'=>'cole o código aqui', 'class'=>'register_token_field']) !!} --}}
        <div class="input-field col s8">
          {!! Form::text('name', null) !!}
          {!! Form::label('name', 'Nome Completo:') !!}
        </div>
        <div class="col s4"></div>
      </div>
      <div class="row">
        <div class="input-field col s4">
          {!! Form::radio('juridical', 'yes'); !!}
          {!! Form::radio('juridical', 'no', true); !!}
          {!! Form::label('', 'Pessoa Jurídica?') !!}
        </div>
        <div class="input-field col s8">
          {!! Form::text('cpf', null) !!}
          {!! Form::label('cpf', 'CPF:') !!}
        </div>
        <div class="input-field col s8 hide">
          {!! Form::text('cnpj', null) !!}
          {!! Form::label('cnpj', 'CNPJ:') !!}
        </div>
      </div>
      <div class="row">
        <div class="input-field col s4">
          {!! Form::text('postal_code', null, ['id'=>'postal_code']) !!}
          {!! Form::label('postal_code', 'CEP:') !!}
        </div>
      </div>
      <div class="row">
        <div class="input-field col s8">
          {!! Form::text('address', null) !!}
          {!! Form::label('address', 'Endereço:') !!}
        </div>
      </div>
      <div class="row">
        <div class="input-field col s4">
          {!! Form::text('district', null) !!}
          {!! Form::label('district', 'Bairro:') !!}
        </div>
        <div class="input-field col s4">
          {!! Form::text('city', null) !!}
          {!! Form::label('city', 'Cidade:') !!}
        </div>
      </div>
      <div class="row">
        <div class="input-field col s4">
          {!! Form::select('size', [
            'AC'=>'Acre',
            'AL'=>'Alagoas',
            'AP'=>'Amapá',
            'AM'=>'Amazonas',
            'BA'=>'Bahia',
            'CE'=>'Ceará',
            'DF'=>'Distrito Federal',
            'ES'=>'Espírito Santo',
            'GO'=>'Goiás',
            'MA'=>'Maranhão',
            'MT'=>'Mato Grosso',
            'MS'=>'Mato Grosso do Sul',
            'MG'=>'Minas Gerais',
            'PA'=>'Pará',
            'PB'=>'Paraíba',
            'PR'=>'Paraná',
            'PE'=>'Pernambuco',
            'PE'=>'Piauí',
            'RJ'=>'Rio de Janeiro',
            'RN'=>'Rio Grande do Norte',
            'RS'=>'Rio Grande do Sul',
            'RO'=>'Rondônia',
            'RR'=>'Roraima',
            'SC'=>'Santa Catarina',
            'SP'=>'São Paulo',
            'SE'=>'Sergipe',
            'TO'=>'Tocantins'
          ], null, ['placeholder' => '=Estado=']); !!}
          {!! Form::label('district', 'Estado:') !!}
        </div>
        <div class="input-field col s4">
          {!! Form::text('country', null) !!}
          {!! Form::label('country', 'País:') !!}
        </div>
        <?php // TODO: LOCALIZAÇãO ?>
      </div>
      <div class="row">
        <div class="input-field col s4">
          {!! Form::date('birthdate', \Carbon\Carbon::now()); !!}
          {!! Form::label('birthdate', 'Data de Nascimento:') !!}
        </div>
        <div class="input-field col s4">
          {!! Form::text('phone', null, ['placeholder'=>'(99)99999-9999']) !!}
          {!! Form::label('phone', 'Telefone:') !!}
        </div>
      </div>
      <div class="row">
        <div class="input-field col s4">
          {!! Form::text('email', $user['email']) !!}
          {!! Form::label('email', 'E-mail:') !!}
        </div>
        <div class="input-field col s4">
          {!! Form::password('password', $user['password']) !!}
          {!! Form::label('password', 'Senha:') !!}
        </div>
      </div>




      <div class="row">
        <div class="col s12 center">
          <a id="btn-register-user" class="btn-large waves-effect waves-light">Verificar <i class="right  material-icons">done</i></a>
        </div>
      </div>
    </div>
  {!! Form::close() !!}
<script>
$("#btn-register-user").click(function(){
  $("#formUserRegister").submit();
})
</script>
@endsection
