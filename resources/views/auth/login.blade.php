@extends('full')
@section('contentFull')
  <div class="vertical-center container">
    <div class="white">
      <div class="row">
        <div class="col s12 center">
          <h4>Login <i class="material-icons">login</i></h4>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col s12">
            <ul id="tabs-swipe-demo" class="tabs">
              <li class="tab col s6"><a class="active" href="#aba-fotografo">Fotografo</a></li>
              <li class="tab col s6"><a href="#aba-cliente">Cliente</a></li>
            </ul>
            <div id="aba-fotografo" class="col s12">
              <div class="row"></div>
              <div class="row"></div>
              {{-- @if (Auth::guard('web')->guest())
                  <!--Seller Login and registration Links -->
                  {{-- <li><a href="{{ url('/seller_register') }}">Seller Registration</a></li>
              @else
                <li><a href="">USER Login</a></li>
              @endif --}}
              {!! Form::open(['method'=>'POST', 'route'=> 'login']) !!}
                <div class="row">
                  <div class="input-field col s12 {{ $errors->has('email') ? ' error' : '' }}">
                    {!! Form::text('email', old('email'), ['required', 'autofocus']) !!}
                    {!! Form::label('email', 'Email:') !!}
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s6 {{ $errors->has('password') ? ' error' : '' }}">
                    {!! Form::password('password', ['required'=>'required']) !!}
                    {!! Form::label('password', 'Senha:') !!}
                  </div>
                  <div class="input-field col s6 {{ $errors->has('password') ? ' error' : '' }}">
                    <p>
                      {!! Form::checkbox('remember', 'yes', false, ['id'=>'remember']) !!}
                      {!! Form::label('remember', 'Lembrar?') !!}
                      <?php // TODO: class add TOP:0 ?>
                    </p>
                  </div>
                </div>
                <div class="row">
                  <div class="col s6">
                    {!! Form::submit('Logar', ['class'=>'btn']) !!}
                  </div>
                </div>
                <div class="row">
                  <div class="col s4">
                    <a href="#">Esqueci a senha</a>
                  </div>
                </div>
              {!! Form::close() !!}
            </div>
            <div id="aba-cliente" class="col s12">
              <div class="row"></div>
              <div class="row"></div>
              {{-- @if (Auth::guard('web_client')->guest())
              @else
                <li><a href="">CLIENTE Login</a></li>
              @endif --}}
              {!! Form::open(['method'=>'POST', 'route'=> 'route_cliente_login']) !!}
                <div class="row">
                  <div class="input-field col s12 {{ $errors->has('email') ? ' error' : '' }}">
                    {!! Form::text('email', old('email'), ['required']) !!}
                    {!! Form::label('email', 'Email:') !!}
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s6 {{ $errors->has('password') ? ' error' : '' }}">
                    {!! Form::password('password', ['required'=>'required']) !!}
                    {!! Form::label('password', 'Senha:') !!}
                  </div>
                  <div class="input-field col s6 {{ $errors->has('password') ? ' error' : '' }}">
                    <p>
                      {!! Form::checkbox('remember_client', 'yes', false, ['id'=>'remember']) !!}
                      {!! Form::label('remember_client', 'Lembrar?') !!}
                      <?php // TODO: class add TOP:0 ?>
                    </p>
                  </div>
                </div>
                <div class="row">
                  <div class="col s6">
                    {!! Form::submit('Logar', ['class'=>'btn']) !!}
                  </div>
                </div>
                <div class="row">
                  <div class="col s4">
                    <a href="#">Esqueci a senha</a>
                  </div>
                </div>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
