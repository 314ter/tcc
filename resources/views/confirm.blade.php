@extends('full')
@section('contentFull')

  {!! Form::open(['method'=>'POST', 'action'=>'UserController@store', 'id'=>'formUserRegister']) !!}
  {!! Form::hidden('email', $user['email']) !!}
  <div class="vertical-center">
    <div class="white">
      <div class="row">
        <div class="col s12 center">
          <img height="150rem" src="{{ asset('images/LOGO.png') }}" alt="">
        </div>
      </div>
      @if(count($errors)>0)
        <div class="row">
          <div class="col s0 m3 l3"></div>
          <div class="col s12 m6 l6">
            <div class="card orange lighten-1">
              <div class="card-content black-text">
                <span class="card-title">Ops :(</span>
                @foreach ($errors->all() as $error)
                  <p>{{ $error }}</p>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      @endif
      <div class="row">
        <div class="col s12">
          <h5 class="center-align invalid">
            Seu registro está quase completo...
          </h5>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col s12">
            <h6>Olá <strong>{{ $user['name'] }}</strong>,</h6>
          </div>
        </div>
        <div class="row">
          <div class="col s12">
            <h6 class="recuo">Um e-mail foi enviado para <strong>{{ $user['email'] }}</strong>, agora você precisa confirmar o seu cadastro, clicando no link que lhe enviamos :)</h6>
            <h6 class="recuo"><i>Verifique sua caixa de SPAM, talvez o e-mail tenha parado lá.</i></h6>
          </div>
        </div>
      </div>
    </div>
  </div>

    {!! Form::close() !!}

@endsection
