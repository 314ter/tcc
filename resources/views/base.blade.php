<!DOCTYPE html>
<html lang="pt">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
  <title>[:o:] [Photobooker]</title>
  <link rel="icon" href="images/favicon.png" type="image/x-icon"/>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  {!! MaterializeCSS::include_full() !!}
  <link type="text/css" rel="stylesheet" href="{{ asset('css/home.css') }}">
</head>
<body>
  <div class="bodyHome">
    @yield('navbar')
    @yield('body')
    @yield('footer')
  </div>
  <script src="{{asset('js/home.js')}}"></script>
  <script src="{{asset('js/init.js')}}"></script>
</body>
</html>
