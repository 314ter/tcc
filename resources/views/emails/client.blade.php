<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="margin: 0;font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;">
<head style="margin: 0;font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;">
<meta name="viewport" content="width=device-width" style="margin: 0;font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" style="margin: 0;font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;box-sizing: border-box;font-size: 14px;">
<title>Confirmando cadastro no [:o:]Photobooker</title>
</head>
<body itemscope itemtype="http://schema.org/EmailMessage" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;width: 100% !important;">
	<!-- Wrapper/Container Table: Use a wrapper table to control the width and the background color consistently of your email. Use this approach instead of setting attributes on the body tag. -->
	<table cellpadding="0" width="100%" cellspacing="0" border="0" id="backgroundTable" class="bgBody" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 0;width: 100% !important;line-height: 100% !important;">
	<tr>
		<td style="border-collapse: collapse;">
	<table cellpadding="0" width="620" class="container" align="center" cellspacing="0" border="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
	<tr>
		<td style="border-collapse: collapse;">
		<!-- Tables are the most common way to format your email consistently. Set your table widths inside cells and in most cases reset cellpadding, cellspacing, and border to zero. Use nested tables as a way to space effectively in your message. -->
		<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container intern-table" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #1b3650;">
			<tr>
				<td class="movableContentContainer bgItem" style="border-collapse: collapse;">
					<div class="movableContent">
						<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
							<tr height="40">
								<td width="200" style="border-collapse: collapse;">&nbsp;</td>
								<td width="200" style="border-collapse: collapse;">&nbsp;</td>
								<td width="200" style="border-collapse: collapse;">&nbsp;</td>
							</tr>
							<tr>
								<td width="200" valign="top" style="border-collapse: collapse;">&nbsp;</td>
								<td width="200" valign="top" align="center" style="border-collapse: collapse;">
									<div class="contentEditableContainer contentImageEditable">
	                	<div class="contentEditable" align="center">
                  		<img src="http://www.photobooker.com.br/images/LOGO.png" width="155" height="155" alt="Logo" data-default="placeholder" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;">
	                	</div>
	              	</div>
								</td>
								<td width="200" valign="top" style="border-collapse: collapse;">&nbsp;</td>
							</tr>
							<tr height="25">
								<td width="200" style="border-collapse: collapse;">&nbsp;</td>
								<td width="200" style="border-collapse: collapse;">&nbsp;</td>
								<td width="200" style="border-collapse: collapse;">&nbsp;</td>
							</tr>
						</table>
					</div>
					<div class="movableContent">
						<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
							<tr>
								<td width="100%" colspan="3" align="center" style="padding-bottom: 10px;padding-top: 25px;border-collapse: collapse;">
									<div class="contentEditableContainer contentTextEditable">
	                	<div class="contentEditable" align="center">
	                  		<h3 style="font-family: Helvetica, Arial, sans-serif;line-height: 22px;font-weight: normal;color: white !important;">Confirmação de cadastro</h3>
	                	</div>
	              	</div>
								</td>
							</tr>
							<tr>
								<td width="100" style="border-collapse: collapse;">&nbsp;</td>
								<td width="400" align="center" style="border-collapse: collapse;">
									<div class="contentEditableContainer contentTextEditable">
	                	<div class="contentEditable" align="left">
	                  		<p style="margin: 1em 0;color: #fff;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 160%;">Olá <strong>{{ $client['name'] }}</strong>,</p>
												<p style="margin: 1em 0;color: #fff;font-family: Helvetica, Arial, sans-serif;font-size: 16px;line-height: 160%;">Parece que {{ $user['name'] }} lhe cadastrou no [:o:] Photobooker, agora você está fazendo parte da nossa família <3
												Para completar seu cadastro, e cadastrar uma senha, basta clicar no botão abaixo :)</p>
	                	</div>
	              	</div>
								</td>
								<td width="100" style="border-collapse: collapse;">&nbsp;</td>
							</tr>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
							<tr>
								<td width="200" style="border-collapse: collapse;">&nbsp;</td>
								<td width="200" align="center" style="padding-top: 25px;border-collapse: collapse;">
									<table class="" cellpadding="0" cellspacing="0" border="0" align="center" width="400" height="50" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
										<tr>
											<td class="tokenAccess" align="center" width="400" height="50">
												<div class="contentEditableContainer contentTextEditable">
				                	<div class="contentEditable" align="center">
														<div class="row center">
															<a href="{{ "http://www.photobooker.com.br/cliente/".$client['email']."/confirmacao"}}" itemprop="url" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; background-color: #26a69a; margin: 0; border-color: #26a69a; border-style: solid; border-width: 10px 20px;">
																Continuar cadastro</a>
									          </div>
				                	</div>
				              	</div>
											</td>
										</tr>
									</table>
								</td>
								<td width="200" style="border-collapse: collapse;">&nbsp;</td>
							</tr>
						</table>
					</div>
					<div class="movableContent">
						<table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
							<tr>
								<td width="100%" colspan="2" style="padding-top: 65px;border-collapse: collapse;">
									<hr style="height:1px;border:none;color:#333;background-color:#ddd;">
								</td>
							</tr>
							<tr>
								<td width="60%" height="30" valign="middle" style="padding-bottom: 10px;border-collapse: collapse;">
									<div class="contentEditableContainer contentTextEditable">
	                	<div class="contentEditable" align="center">
											<br>
											<span style="font-size:11px;color:#000;font-family:Helvetica, Arial, sans-serif;line-height:200%; align:center">Equipe [:o:]Photobooker <strong>|</strong> www.photobooker.com.br</span>
											<br>
											<br>
                		</div>
              		</div>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>
		</td>
	</tr>
	</table>
</body>
</html>
